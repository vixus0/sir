#!/bin/bash

BLENDER=${BLENDER:-"blender"}
LIBS=${LIBS:-"$LD_LIBRARY_PATH"}
FBXCONV=${FBXCONV:-"./tools/fbx-conv-lin64"}
G3OUT=${G3OUT:-"G3DB"}
assets="${assets:-"android/assets"}"
levels="levels"
models="models"
g3ext="${G3OUT,,}"

pushd `dirname $0` > /dev/null

# Build models
for f in `find ${models}/ -name "*.blend" -print`; do
  base="${f#*/}"
  out="${assets}/${base%.*}.$g3ext" 

  # Skip if converted file already exists and is newer
  test -e "$out" -a "$out" -nt "$f" && continue

  # Remove old file
  rm "$out" 2>/dev/null

  # Otherwise do the conversion
  $BLENDER $f --background --python ./tools/blend2fbx.py -- tmp.fbx 2>&1 >/dev/null
  LD_LIBRARY_PATH="$LIBS:./tools" $FBXCONV -f -o $G3OUT tmp.fbx >/dev/null
  rm -f tmp.fbx

  if test -e "tmp.$g3ext"; then
    mkdir -p $(dirname $out)
    mv "tmp.$g3ext" "$out"
    echo "Converted $f --> $out"
  else
    echo "Error converting $f"
    exit
  fi
done

# Build levels
for f in ${levels}/*.txt; do
  base="${f##*/}"
  out="${assets}/levels/${base%.*}.sir" 

  # Skip if converted file already exists and is newer
  test -e "$out" -a "$out" -nt "$f" && continue

  # Remove old file
  rm "$out" 2>/dev/null

  # Otherwise do the conversion
  ./tools/txt2sir $f $out

  if test -e "$out"; then
    echo "Converted $f --> $out"
  else
    echo "Error converting $f"
  fi
done

# Generate sir logo
test ! -f $assets/sirlogo.png && inkscape -z -e $assets/sirlogo.png sirlogo.svg

# Copy over textures
cp models/player/*.png ${assets}/player/
cp models/props/*.png ${assets}/props/
cp models/tiles/*.png ${assets}/tiles/
cp UI/{default,uiskin}.* ${assets}/

popd > /dev/null
