package com.walrus.sir;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureAdapter;
import com.badlogic.gdx.math.Vector3;

/**
 * The Player controls an Actor and is intended to be the main and
 * only controllable character (for now).
 */
public class Player implements CollisionListener {

	private Actor actor;
	private GestureDetector gestures;

	public Player(Actor actor) {
		this.actor = actor;

		gestures = new GestureDetector(new GestureAdapter() {
			@Override
			public boolean fling(float vx, float vy, int btn) {
				changeDirection(vx,vy);
				return true;
			}

			@Override
			public boolean tap(float x, float y, int count, int button) {
				// single tap = jump
				if(count == 1) {
					jump();
				}

				return true;
			}

		});

		// Add Player class as a listener for collisions with actor
		actor.addCollisionListener(this);

		// Play idle animation indefinitely
		actor.playAnimation("Armature|idle", 1f, -1);
	}

	/**
	 * Changes the player direction according to some velocity.
	 */
	public void changeDirection(float vx, float vy) {
		if(playerIsStandingOn() != LevelBuilder.CollisionCategory.nothing) {
			// Give the Player velocity in the flung direction
			// and make player face that direction
			final float max_speed = 10;
			actor.physObj.body.activate(true);
			Vector3 dir = new Vector3(-vx, 0f, vy); // ffs why does this have to be flipped in the y?
			dir.nor();
			actor.physObj.faceDirection(dir);

			Vector3 vel = new Vector3(-vx, 0f, -vy);
			vel.nor().scl(max_speed);
			actor.physObj.body.setLinearVelocity(vel);

			// Now that player is moving play run animation
			actor.playAnimation("Armature|run", 5f, -1);
		}
	}

	/** Jump if player is standing on anything but 'nothing'*/
	public void jump() {
		if(playerIsStandingOn() != LevelBuilder.CollisionCategory.nothing) {
			final float jump_impulse = 50;
			actor.playAction("Armature|jump", 1f, 1, 0.5f);
			actor.physObj.body.activate(true);
			actor.physObj.body.applyCentralImpulse(new Vector3(0, jump_impulse, 0));
		} else {
			Gdx.app.log("Sir!", "Can't jump; Standing on nothing.");
		}
	}

	public LevelBuilder.CollisionCategory playerIsStandingOn() {
		final Vector3 rayDiff = new Vector3(0,-1.3f,0);
		Vector3 A = actor.physObj.getPos();
		Vector3 B = new Vector3(A).add(rayDiff);
		return ScrGame.sirLevel.physics.rayCasterProXL(A, B);
	}

	public Actor getActor() {
		return actor;
	}

	/**
 	* Adds this object's controllers to the global input multiplexer
 	*/
	public void handleInputs() {
		Sir.inputs.addProcessor(gestures);
	}

//	public GestureAdapter getController() {
//		return gestures;
//	}

	/**
	 * Kick the object player has collided with
	 */
	public void collision(Collidable who, Collidable collidedWith) {
		PhysObject.kick(who.getPhysObject(), collidedWith.getPhysObject(), 30f);
		actor.playAction("Armature|kick", 2f, 1, 0.3f);
		((Actor)collidedWith).playAnimation("Armature|break", 1f, 1);
	}

	public Vector3 getPos() {
		return actor.physObj.getPos();
	}
}
