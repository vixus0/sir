package com.walrus.sir;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.collision.btBoxShape;
import com.badlogic.gdx.physics.bullet.collision.btSphereShape;
import com.badlogic.gdx.physics.bullet.collision.btCollisionShape;
import com.badlogic.gdx.math.Matrix4;

public class LevelBuilder {
	public static final float floorHeight = 2.0f;

	public static enum AIAlgo { none, customer, waiter, chef, head_chef, jason_crain, fish }
	public static enum PropState { staticprop, dynamicprop, quasiprop }
	public static enum CollisionCategory { nothing, floor, wall, stairs, prop, player }
	public static enum CollisionShape { hull, box, sphere, capsule }


	/**
	 * Given a loaded SirBinaryFileReader instance, this will build a level out of the info, returning the level
	 * data as a SirLevel object.
	 */
	public static SirLevel build(SirBinaryFileReader sbfr) {

		// Create new SirLevel object to hold all level file related stuff
		final SirLevel sirLevel = new SirLevel();

		// Build all the floors
		Model all_tiles = Sir.assets.get(sbfr.getTilesFname());
		for(int f = 0; f < sbfr.getNumFloors(); f++) {
			buildFloor(sirLevel, sbfr, all_tiles, f);
		}

		// Build all the props
		for(int i = 0; i < sbfr.getNumProps(); i++) {
			String propId = sbfr.getPropId(i);
			String propFname = "props/" + propId + ".g3db";
			Model propModel = Sir.assets.get(propFname);
			float yaw = sbfr.getPropRotation(i);
			Vector3 pos = sbfr.getPropPosition(i);
			Vector3 rot = new Vector3(yaw, 0f, 0f);
			LevelBuilder.PropState propstate = sbfr.getPropState(i);
			LevelBuilder.AIAlgo ai_algo = sbfr.getPropAI(i);

			// Build prop and add its model instance to the instances array
			Actor newActor;
			if(ai_algo == LevelBuilder.AIAlgo.none) {
				newActor = new Actor(propId, propModel, pos, rot, propstate, LevelBuilder.CollisionCategory.prop);
			} else {
				newActor = new Actor(propId, propModel, pos, rot, new btSphereShape(.5f), 30f, LevelBuilder.CollisionCategory.prop);
			}
			sirLevel.instances.add(newActor.instance);
			sirLevel.actors.add(newActor);

			// Add this tile to the bullet world for collision purposes
			sirLevel.physics.addCollidable(newActor, Physics.COL_PROP, Physics.COL_ALL, Physics.COL_NOTHING);

			// Check if prop has some AI algorithm associated with it
			if(ai_algo != LevelBuilder.AIAlgo.none) {

				// Assign the specified AI algorithm to control this actor
				AI newAI;
				switch(sbfr.getPropAI(i)) {
					case customer:
						newAI = new AICustomer(newActor);
					break;
					case waiter:
						newAI = new AIWaiter(newActor);
					break;
					case chef:
						newAI = new AIChef(newActor);
					break;
					case head_chef:
						newAI = new AIHeadChef(newActor);
					break;
					case jason_crain:
						newAI = new AICrackdown(newActor);
					break;
					case fish:
						newAI = new AIFish(newActor);
					break;
					default:
						Gdx.app.error("Sir!", "AIAlgo state not implemented.");
						return null;
				}

				// Add new AI to list of AIs
				sirLevel.ais.add(newAI);
			}
		}

		// Spawn player
		Vector3 start = sbfr.getPlayerStartPosition();
		LevelBuilder.createPlayer(sirLevel, start);

        // This stuff should eventually be specified by the level file
        sirLevel.env.set(new ColorAttribute(ColorAttribute.AmbientLight, 1.0f, 1.0f, 1.0f, 1f));

		return sirLevel;
	}

	/**
	 * Private helper function for Build(). Only separated to make code more readable.
	 */
	private static void buildFloor(SirLevel sirLevel, SirBinaryFileReader sbfr, Model all_tiles, int floorIndex) {
		// Build all the tiles for this floor
		final int floor_x = sbfr.get_floor_x(floorIndex);
		final int floor_y = sbfr.get_floor_y(floorIndex);
		final int first_tile_x = sbfr.get_first_tile_x(floorIndex);
		final int first_tile_y = sbfr.get_first_tile_y(floorIndex);
		for(int y = 0; y < floor_y; y++) {
			for(int x = 0; x < floor_x; x++) {
				if(sbfr.tileNotEmpty(floorIndex, x, y)) {
					String nodeId = sbfr.getTileId(floorIndex, x, y);
					float yaw = sbfr.getTileRotation(floorIndex, x, y);
					Vector3 pos = new Vector3((x + first_tile_x) * 2f, floorIndex * floorHeight, (y + first_tile_y) * 2f);
					Vector3 rot = new Vector3(0f, 0f, yaw);

					// Set collision shape based on tile collision category
					btCollisionShape colshape;
					LevelBuilder.CollisionCategory collisionCategory = sbfr.getTileCollision(floorIndex,x,y);

					//switch(collisionCategory) {
					//	case wall:
					//		colshape = new btBoxShape(new Vector3(1f, 2.5f, 1f));
					//		break;
					//	case floor:
					//		colshape = new btBoxShape(new Vector3(1f, floor_thickness*.5f, 1f));
					//		break;
					//	case stairs:
					//		//final Model stairsCol = Sir.assets.get("collision/stairsCol.g3db");
					//		colshape = createConvexHullShape(all_tiles, nodeId, true);
					//		//Sir.instances.add(stairsCol);
					//		break;
					//	default:
					//		Gdx.app.error("Sir!", "TileCollision - No collision shape implemented for given tile collision category.");
					//		levelBuilt = false;
					//		return;
					//}
					colshape = PhysObject.createConvexHullShape(all_tiles, nodeId, true);

					// Build tile and add its model instance to the instances array
					Actor newActor = new Actor("tile", all_tiles, nodeId, pos, rot, colshape, 0f, collisionCategory);
					sirLevel.instances.add(newActor.instance);
					sirLevel.actors.add(newActor);

					// Add this tile to the bullet world for collision purposes
					sirLevel.physics.addCollidable(newActor, Physics.COL_TILE, Physics.COL_DYNAMIC_AND_RAYCASTABLE, Physics.COL_NOTHING);
				}
			}
		}
	}

	/**
	* Create player at certain starting position in the level.
	*/
	private static void createPlayer(SirLevel sirLevel, Vector3 start) {
		// Create a new Actor using the kid g3db and a sphere collision shape
		Model playerModel = Sir.assets.get("player/kid_game.g3db");
		Actor newActor = new Actor("player", playerModel, start, new Vector3(0f, 0f, 0f), new btSphereShape(.5f), 10f, LevelBuilder.CollisionCategory.player);

		// Prevent ModelInstance rolling along with the bullet sphere
		newActor.disableInstanceRotation();

		// Add to various lists concerning graphics and physics
		sirLevel.instances.add(newActor.instance);
		sirLevel.physics.addCollidable(newActor, Physics.COL_PLAYER, Physics.COL_ALL_EXCEPT_RAYCAST, Physics.COL_PROP);
		sirLevel.actors.add(newActor);

		// Create the Player controller for this Actor, and chase it with the camera
		sirLevel.player = new Player(newActor);
	}
}
