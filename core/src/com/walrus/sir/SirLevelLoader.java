package com.walrus.sir;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.assets.loaders.AsynchronousAssetLoader;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.utils.GdxRuntimeException;

/**
 * AssetLoader for SirLevel instances. The SirLevel is loaded asynchronously.
 */
public class SirLevelLoader extends AsynchronousAssetLoader<SirLevel, SirLevelLoader.SirLevelParameter> {

  private SirBinaryFileReader sbfr;
  private SirLevel sirLevel;

  public SirLevelLoader () {
    this(new InternalFileHandleResolver());
  }

  public SirLevelLoader (FileHandleResolver resolver) {
    super(resolver);
  }

  @Override
  public Array<AssetDescriptor> getDependencies(String fileName, FileHandle file, SirLevelParameter parameter) {
    Sir.levelLoadingProgress = 0f;

    // Read the binary Sir file, and throw exception if that fails for whatever reason
    sbfr = new SirBinaryFileReader(fileName);
    if (!sbfr.fileSuccessfullyLoaded()) {
      throw new GdxRuntimeException("Sir binary file object '" + fileName  + "' was not correctly loaded.");
    }

    Sir.levelLoadingProgress = .1f;

    // Get the list of dependencies the sbfr has found references to
    final Array<AssetDescriptor> deps = sbfr.getDependencies();

    Sir.levelLoadingProgress = .2f;

    return deps;
  }

  @Override
  public void loadAsync(AssetManager manager, String fileName, FileHandle file, SirLevelParameter parameter) {
    Sir.levelLoadingProgress = .8f;
    sirLevel = LevelBuilder.build(sbfr);
    if(sirLevel == null) {
      throw new GdxRuntimeException("Error when building level for '" + fileName  + "'.");
    }
    Sir.levelLoadingProgress = 1f;
  }

  /**
   * Loads the debug drawer in loadSync(), because it needs OpenGL context
   */
  @Override
  public SirLevel loadSync(AssetManager manager, String fileName, FileHandle file, SirLevelParameter parameter) {
    // Irritatingly, the debug drawer has to be initialised in the loadSync() because it requires the OpenGL context
    this.sirLevel.physics.initDebugDrawer();

    Gdx.app.log("Sir!",""+sbfr);

    SirLevel returnSirLevel = this.sirLevel;
    this.sirLevel = null;
    this.sbfr = null;
    return returnSirLevel;
  }

  static public class SirLevelParameter extends AssetLoaderParameters<SirLevel> {
  }
}
