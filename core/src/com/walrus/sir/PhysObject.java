package com.walrus.sir;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.model.MeshPart;
import com.badlogic.gdx.graphics.g3d.model.Node;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.physics.bullet.collision.btBoxShape;
import com.badlogic.gdx.physics.bullet.collision.btCollisionShape;
import com.badlogic.gdx.physics.bullet.collision.btConvexHullShape;
import com.badlogic.gdx.physics.bullet.collision.btShapeHull;
import com.badlogic.gdx.physics.bullet.collision.btSphereShape;
import com.badlogic.gdx.physics.bullet.dynamics.btRigidBody;
import com.badlogic.gdx.physics.bullet.linearmath.btMotionState;
import com.badlogic.gdx.utils.BufferUtils;
import com.badlogic.gdx.utils.Disposable;

import java.nio.FloatBuffer;


class PhysObject implements Disposable {

	public final btRigidBody body;
	public final MotionState motionState;
	public final float mass;
	public final float friction;

	private class MotionState extends btMotionState {
		Matrix4 transform;
		private Vector3 translation;
		private Matrix4 transformFixedRotation;
		private boolean sansRotation;

		public MotionState() {
			this.sansRotation = false;
			this.translation = new Vector3();
			this.transformFixedRotation = new Matrix4();
		}

		@Override
		public void getWorldTransform (Matrix4 worldTrans) {
			worldTrans.set(transform);
		}
		@Override
		public void setWorldTransform (Matrix4 worldTrans) {
			if(sansRotation) {
				transform.set(transformFixedRotation);
				worldTrans.getTranslation(translation);
				transform.trn(translation);
			} else {
				transform.set(worldTrans);
			}
		}

		/**
		 * Prevents ModelInstance receiving rotational state of bullet object, translation only.
		 */
		public void disableInstanceRotation() {
			this.sansRotation = true;
		}

		public void faceDirection(Vector3 dir) {
			transformFixedRotation.setToLookAt(dir, Physics.UP);
		}
	}

	/**
	 * Create a PhysObject from the collision info in the given model instance
	 */
	public PhysObject(ModelInstance instance, LevelBuilder.PropState propstate) {
		friction = Physics.friction;

		final btCollisionShape shape;
		final btRigidBody.btRigidBodyConstructionInfo info;
		final Vector3 inertia = new Vector3();

		// Link instance transform and bullet transform
		motionState = new MotionState();
		motionState.transform = instance.transform;

		// Check if instance contains collision node
		final Node collisionNode = instance.getNode("collision");

		float massDef = 5f;

		if (collisionNode != null) {
			// Get the collision mesh (first child node of collision node)
			MeshPart collMeshPart = collisionNode.getChild(0).parts.get(0).meshPart;
			Mesh collMesh = collMeshPart.mesh;

			// Get the mass (name of second child node)
			String massString = collisionNode.getChild(1).id;
			massDef = Float.parseFloat(massString);

			// Load up all the vertices in the collision mesh
			int nverts = collMesh.getNumVertices();
			int vsize = collMesh.getVertexSize();
			float[] verts = new float[nverts*vsize];
			collMesh.getVertices(verts);

			// Different collision shapes depending on number of vertices in collision mesh
			switch(nverts) {
				case 2:
					// Sphere
					float r = Vector3.dst2(verts[0], verts[1], verts[2], verts[3], verts[4], verts[5]);
					shape = new btSphereShape(r);
					break;

				case 8:
					// Box
					BoundingBox box = collMesh.calculateBoundingBox();
					Vector3 dims = new Vector3();
					box.getDimensions(dims);
					shape = new btBoxShape(dims);
					break;

				default:
					// Convex hull
					shape = createConvexHullShape(collMeshPart, true);
					break;
			}
		} else {
			final MeshPart part;
			BoundingBox box = new BoundingBox();
			Vector3 dims = new Vector3();

			try {
				part = instance.getNode("visible").parts.get(0).meshPart;
				part.mesh.calculateBoundingBox(box, part.indexOffset, part.numVertices);
				box.getDimensions(dims);
			} catch (NullPointerException e) {
				Gdx.app.log("Sir!","PhysObject - Model missing visible node!");
				dims.set(5f, 5f, 5f);
			}

			Gdx.app.log("Sir!", "PhysObject - " + String.format("%f %f %f", dims.x, dims.y, dims.z));

			shape = new btBoxShape(dims);
		}


		// Check the prop state:
		// If it's static, set the mass to zero, so it can never move
		// If it's dynamic, set the mass positive, and leave the object active
		// If it's quasistatic, set the mass positive, but set the object initially inactive (until it experiences a collision)
		switch(propstate)
		{
			case staticprop:
				mass = 0f;
				break;

			case dynamicprop:
			case quasiprop:

				mass = massDef;
				break;

			default:
				mass = 0f;
				break;
		}


		// calc rotational inertia
		if (mass > 0f) {
			shape.calculateLocalInertia(mass, inertia);
		}
		else {
			inertia.setZero();
		}

		info = new btRigidBody.btRigidBodyConstructionInfo(mass, null, shape, inertia);
		info.setFriction(friction);

		body = new btRigidBody(info);
		body.setMotionState(motionState);

		if (propstate == LevelBuilder.PropState.quasiprop) {
			body.setActivationState(0);
		} else if (propstate == LevelBuilder.PropState.dynamicprop) {
			body.setActivationState(1);
		}

		info.dispose();
	}


	/**
	 * Create a PhysObject for the given collision shape and mass
	 */
	public PhysObject(ModelInstance instance, btCollisionShape shape, float mass) {
		friction = Physics.friction;
		this.mass = mass;

		final btRigidBody.btRigidBodyConstructionInfo info;
		final Vector3 inertia = new Vector3();

		// Link instance transform and bullet transform
		motionState = new MotionState();
		motionState.transform = instance.transform;

		if (mass > 0f) {
			shape.calculateLocalInertia(mass, inertia);
		}
		else {
			inertia.setZero();
		}

		info = new btRigidBody.btRigidBodyConstructionInfo(mass, null, shape, inertia);
		info.setFriction(friction);

		body = new btRigidBody(info);
		body.setMotionState(motionState);

		info.dispose();
	}

	/**
	 * Returns the current position of this object.
	 */
	public Vector3 getPos() {
		Vector3 tempPos = new Vector3();
		motionState.transform.getTranslation(tempPos);
		return tempPos;
	}

	/**
	 * Applies an upwards force from one PhysObject to another.
	 */
	public static void kick(PhysObject from, PhysObject to, float force) {
		Vector3 v = from.getPos().sub(to.getPos()).cpy();
		to.body.applyForce(new Vector3(0f, force, 0f), v.sub(0f,1f,0f));
	}

	/**
	* Prevents ModelInstance receiving rotational state of bullet object, translation only.
	*/
	public void disableInstanceRotation() {
		motionState.disableInstanceRotation();
	}

	public void faceDirection(Vector3 dir) {
		motionState.faceDirection(dir);
	}

	@Override
	public void dispose() {
		body.dispose();
		motionState.dispose();
	}

  /**
   * Creates a convex hull collision shape from an arbitrary model node.
   */
	public static btConvexHullShape createConvexHullShape (final Model model, String nodeId, boolean optimize) {
   		return createConvexHullShape(model.getNode(nodeId).parts.get(0).meshPart, optimize);
	}

	public static btConvexHullShape createConvexHullShape (final MeshPart mp, boolean optimize) {
   		final short[] idx = new short[mp.numVertices];

   		mp.mesh.getIndices(mp.indexOffset, mp.numVertices, idx, 0);

   		int stride = mp.mesh.getVertexSize()/4;
   		float[] v = new float[stride];
   		int offset, count;

   		final FloatBuffer fb = BufferUtils.newFloatBuffer(mp.numVertices*stride);

   		for (short i : idx) {
      		offset = i * stride;
   	  		mp.mesh.getVertices(offset, stride, v);
   			fb.put(v);
   		}

   		final btConvexHullShape result;
   		final btConvexHullShape shape = new btConvexHullShape(fb, mp.numVertices, mp.mesh.getVertexSize());

    	if (optimize) {
	    	final btShapeHull hull = new btShapeHull(shape);
    		hull.buildHull(shape.getMargin());
	    	result = new btConvexHullShape(hull);
	    	shape.dispose();
	    	hull.dispose();
    	} else {
			result = shape;
    	}

		return result;
	}
}
