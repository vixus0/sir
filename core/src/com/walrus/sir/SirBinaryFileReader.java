package com.walrus.sir;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.graphics.g3d.Model;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStream;

import com.badlogic.gdx.files.FileHandle;

public class SirBinaryFileReader {

	// Version supported by this SirBinaryFileReader
	final short sirBinaryFileReaderVersion = 13;

	// This version expects the id to be 10 chars
	final int id_length = 10;

	// HEADER
	private short version, num_floors, num_tile_types, num_prop_types, num_props;

	// FILES
	private String tiles_g3db;

	// TILE DEFS
	private class TILE_DEF {
		public String id;
		public LevelBuilder.CollisionCategory collisionCategory;
		public short rotation;
	}
	private TILE_DEF[] tile_def;


	// AI
	private static LevelBuilder.AIAlgo[] ai_algo_map =	{
										LevelBuilder.AIAlgo.none,		// 0
										LevelBuilder.AIAlgo.customer,	// 1
										LevelBuilder.AIAlgo.waiter,		// 2
										LevelBuilder.AIAlgo.chef,		// 3
										LevelBuilder.AIAlgo.head_chef,	// 4
										LevelBuilder.AIAlgo.jason_crain, // 5
										LevelBuilder.AIAlgo.fish		 	// 6
									};

	// PROPSTATES
	private static LevelBuilder.PropState[] propstate_map =	{
										LevelBuilder.PropState.staticprop,	// 0
										LevelBuilder.PropState.dynamicprop,	// 1
										LevelBuilder.PropState.quasiprop		// 2
									};

	// PROP COLLISION SHAPES
	private static LevelBuilder.CollisionShape[] propcoll_map =	{
										LevelBuilder.CollisionShape.hull,	// 0
										LevelBuilder.CollisionShape.box,		// 1
										LevelBuilder.CollisionShape.sphere,	// 2
										LevelBuilder.CollisionShape.capsule	// 3
									};

	// PROP CATEGORIES
	private static LevelBuilder.CollisionCategory[] collisionCategory_map =	{
										LevelBuilder.CollisionCategory.nothing,	// 0
										LevelBuilder.CollisionCategory.floor,	// 1
										LevelBuilder.CollisionCategory.wall,	 	// 2
										LevelBuilder.CollisionCategory.stairs, 	// 3
										LevelBuilder.CollisionCategory.prop,	 	// 4
										LevelBuilder.CollisionCategory.player, 	// 5
									};


	// PROP DEFS
	private class PROP_DEF {
		public String id;
		public LevelBuilder.PropState propstate;
		public LevelBuilder.AIAlgo ai_algo;
		public LevelBuilder.CollisionShape propcoll;
	}
	private PROP_DEF[] prop_def;

	// TILES
	private class FLOOR {
		public short floor_x;
		public short floor_y;
		public short first_tile_x;
		public short first_tile_y;
		public short[][] tiles;
	}
	private FLOOR[] floors;

	// PROPS
	private static class PROP {
		public short index;
		public float posx, posy, posz;
		public float rotation;
	}
	private PROP[] props;

	// PLAYER
	private float player_start_x, player_start_y, player_start_z;

	private String sir_fname;

	private boolean successfullyLoaded = false;

	/* Read data from the specified binary sir file */
	public SirBinaryFileReader(String sir_fname) {
		this(sir_fname, false);
	}

	/** Read data from the specified binary sir file */
	public SirBinaryFileReader(String sir_fname, boolean allow_wrong_version) {

		this.sir_fname = sir_fname;
		Gdx.app.log("Sir!", "SirBinaryFileReader - Reading in " + sir_fname + " binary sir file.");

		try {
			FileHandle fh = Gdx.files.internal(sir_fname);
			InputStream is = fh.read();
			DataInputStream dis = new DataInputStream(is);

			// First check that this is indeed a sir binary file (SIRB)
			byte[] magic_number = new byte[4];
			dis.read(magic_number, 0, 4);
			String magic_str = (new String(magic_number)).trim();
			if(!magic_str.equals("SIRB")) {
				Gdx.app.error("Sir!", "SirBinaryFileReader " + sir_fname + " is not a SIR Binary file. Found magic number '" + magic_str + "'. Should be 'SIRB'");
				successfullyLoaded = false;
				return;
			}

			// Read the version
			version = dis.readShort();

			// Check the version and warn if unsupported
			if(version != sirBinaryFileReaderVersion) {
				Gdx.app.error("Sir!", "SirBinaryFileReader - Error: '" + sir_fname + "' is version " + version + " but this reader handles files of version " + sirBinaryFileReaderVersion + "\nSet allow_wrong_version=true to force reading.");
				if(!allow_wrong_version) {
					successfullyLoaded = false;
					return;
				}
			}

			// Get rest of header
			num_floors = dis.readShort();
			num_tile_types = dis.readShort();
			num_prop_types = dis.readShort();
			num_props = dis.readShort();

			// get g3db model fnames
			byte[] b = new byte[id_length];
			dis.read(b, 0, id_length);
			tiles_g3db = "tiles/" + (new String(b)).trim() + ".g3db";

			// Create the necessary arrays with appropriate dimensions
			tile_def = new TILE_DEF[num_tile_types];
			prop_def = new PROP_DEF[num_prop_types];
			floors = new FLOOR[num_floors];
			props = new PROP[num_props];

			// Read in the tile definitions
			for(int i = 0; i < num_tile_types; i++) {
				// Python stores char as one byte, not two like java
				tile_def[i] = new TILE_DEF();
				dis.read(b, 0, id_length);
				tile_def[i].id = (new String(b)).trim();
				int collisionCategory_index = (int)dis.readByte();
				if (collisionCategory_index < 0 || collisionCategory_index > collisionCategory_map.length) {
					successfullyLoaded = false;
					Gdx.app.error("Sir!", "SirBinaryFileReader - Error: '" + sir_fname + "': collisionCategory_index " + collisionCategory_index + " should lie in range 0 to " + (collisionCategory_map.length-1) + " inclusive.");
					return;
				}
				tile_def[i].collisionCategory = collisionCategory_map[collisionCategory_index];
				tile_def[i].rotation = dis.readShort();
			}

			// Read in the prop definitions
			for(int i = 0; i < num_prop_types; i++) {
				prop_def[i] = new PROP_DEF();
				dis.read(b, 0, id_length);
				prop_def[i].id = (new String(b)).trim();

				int propstate_index = (int)dis.readByte();
				if (propstate_index < 0 || propstate_index > propstate_map.length) {
					successfullyLoaded = false;
					Gdx.app.error("Sir!", "SirBinaryFileReader - Error: '" + sir_fname + "': propstate_index " + propstate_index + " should lie in range 0 to " + (propstate_map.length-1) + " inclusive.");
					return;
				}
				prop_def[i].propstate = propstate_map[propstate_index];

				int ai_algo_index = (int)dis.readByte();
				if (ai_algo_index < 0 || ai_algo_index > ai_algo_map.length) {
					successfullyLoaded = false;
					Gdx.app.error("Sir!", "SirBinaryFileReader - Error: '" + sir_fname + "': ai_algo_index " + ai_algo_index + " should lie in range 0 to " + (ai_algo_map.length-1) + " inclusive.");
					return;
				}
				prop_def[i].ai_algo = ai_algo_map[ai_algo_index];

				int propcoll_index = (int)dis.readByte();
				if (propcoll_index < 0 || propcoll_index > propcoll_map.length) {
					successfullyLoaded = false;
					Gdx.app.error("Sir!", "SirBinaryFileReader - Error: '" + sir_fname + "': propcoll_index " + propcoll_index + " should lie in range 0 to " + (propcoll_map.length-1) + " inclusive.");
					return;
				}
				prop_def[i].propcoll = propcoll_map[propcoll_index];
			}

			// Read in the data for each floor
			for (int f = 0; f < num_floors; f++) {
				// Read in the floor header data (floor_x, floor_y, first_tile_x, first_tile_y)
				floors[f] = new FLOOR();
				floors[f].floor_x = (short)dis.readShort();
				floors[f].floor_y = (short)dis.readShort();
				floors[f].first_tile_x = (short)dis.readShort();
				floors[f].first_tile_y = (short)dis.readShort();

				// Allocate floor map tiles array
				floors[f].tiles = new short[floors[f].floor_x][floors[f].floor_y];

				// Read in the tile indices for this floor map
				for (int y = 0; y < floors[f].floor_y; y++) {
					for (int x = 0; x < floors[f].floor_x; x++) {
						floors[f].tiles[x][y] = (short)dis.readByte();
					}
				}
			}

			// Read in the props
			for(int i = 0; i < num_props; i++) {
				props[i] = new PROP();
				props[i].index = (short)dis.readByte();
				props[i].posx = dis.readFloat();
				props[i].posy = dis.readFloat();
				props[i].posz = dis.readFloat();
				props[i].rotation = dis.readFloat();
			}

			// Read in the player start position
			player_start_x = dis.readFloat();
			player_start_y = dis.readFloat();
			player_start_z = dis.readFloat();

		} catch(Exception e) {
			Gdx.app.error("Sir!", "SirBinaryFileReader - Error when trying to process binary sir file '" + sir_fname + "':" + e);
			e.printStackTrace();
			successfullyLoaded = false;
			return;
		}


		successfullyLoaded = true;
	}


	/**
 	 * Returns an array of descriptors for the assets (models) that need to be loaded in order to build this level.
	 * Note that java will whine intolerably if the AssetDescriptor type isn't specified on construction,
	 * complaining about "unchecked call to AssetDescriptor(String,Class<T>) as a member of the raw type
	 * AssetDescriptor". As all our (top level) dependencies are models, just leave it like this:
	 * deps.add(new AssetDescriptor<Model>("player/kid_game.g3db", Model.class));
	 * for example, and the nastiness goes away. Do not try to specify the type anywhere else, however,
	 * as otherwise that doesn't work either (WHOOOOooo JAVA! U-S-A! U-S-A! U-S-A!)
 	*/
	public Array<AssetDescriptor> getDependencies() {
		// Make sure file was successfully loaded first
		if (!this.fileSuccessfullyLoaded()) {
			Gdx.app.error("Sir!", "SirBinaryFileReader - Sir binary file object was not correctly loaded.");
			return null;
		}

		final Array<AssetDescriptor> deps = new Array<AssetDescriptor>();

		// Make sure Mr. Insolence himself has been loaded
		deps.add(new AssetDescriptor<Model>("player/kid_game.g3db", Model.class));

		// Tiles
		deps.add(new AssetDescriptor<Model>(this.getTilesFname(), Model.class));

		// Prop models
		for (int i = 0; i < this.getNumPropTypes(); i++) {
			deps.add(new AssetDescriptor<Model>("props/" + this.getPropTypeId(i) + ".g3db", Model.class));
		}

		return deps;
	}

	public int get_floor_x(int floorIndex) {
		return floors[floorIndex].floor_x;
	}
	public int get_floor_y(int floorIndex) {
		return floors[floorIndex].floor_y;
	}
	public int get_first_tile_x(int floorIndex) {
		return floors[floorIndex].first_tile_x;
	}
	public int get_first_tile_y(int floorIndex) {
		return floors[floorIndex].first_tile_y;
	}

	/** Check if there is a tile at the given position in the tile array on the given floor */
	public boolean tileNotEmpty(int floorIndex, int i, int j) {
		return (floors[floorIndex].tiles[i][j] != -1);
	}

	/** Return the id of the tile at position i, j in the tile array on the given floor */
	public int getTileIndex(int floorIndex, int i, int j) {
		return floors[floorIndex].tiles[i][j];
	}

	public int getPropIndex(int i) {
		return props[i].index;
	}

	public String getTileId(int i) {
		return tile_def[i].id;
	}

	public String getPropTypeId(int i) {
		return prop_def[i].id;
	}

	public String getPropId(int i) {
		return prop_def[props[i].index].id;
	}

	public LevelBuilder.AIAlgo getPropAI(int i) {
		return prop_def[props[i].index].ai_algo;
	}

	public LevelBuilder.PropState getPropState(int i) {
		return prop_def[props[i].index].propstate;
	}

	public LevelBuilder.CollisionShape getPropCollisionShape(int i) {
		return prop_def[props[i].index].propcoll;
	}

	public String getPropFname(int i) {
		return prop_def[i].id + ".g3db";
	}

	public int getNumTileTypes() {
		return num_tile_types;
	}

	public int getNumFloors() {
		return num_floors;
	}

	public int getNumPropTypes() {
		return num_prop_types;
	}

	public int getNumProps() {
		return num_props;
	}

	/** Return the id of the tile at position i, j in the tile array on the given floor */
	public String getTileId(int floorIndex, int i, int j) {
		return tile_def[floors[floorIndex].tiles[i][j]].id;
	}

	/** Return the rotation of the tile at position i, j in the tile array on the given floor */
	public short getTileRotation(int floorIndex, int i, int j) {
		return tile_def[floors[floorIndex].tiles[i][j]].rotation;
	}

	/** Return the rotation of the specified tile */
	public short getTileRotation(int i) {
		return tile_def[i].rotation;
	}

	public LevelBuilder.CollisionCategory getTileCollision(int floorIndex, int i, int j) {
		return tile_def[floors[floorIndex].tiles[i][j]].collisionCategory;
	}

	public Vector3 getPropPosition(int i) {
		return new Vector3(props[i].posx, props[i].posy, props[i].posz);
	}

	/** Return the rotation of the specified prop */
	public float getPropRotation(int i) {
		return props[i].rotation;
	}

	/** Return player start vector */
	public Vector3 getPlayerStartPosition() {
		return new Vector3(player_start_x, player_start_y, player_start_z);
	}

	/** Calculate bounding box of level, in terms of number of tiles */
	public int[] calcBoundingBox() {
		int max_x=-1, max_y=num_floors, max_z=-1;
		for(int f = 0; f < num_floors; f++) {
			int other_x = floors[f].floor_x + floors[f].first_tile_x;
			int other_y = floors[f].floor_y + floors[f].first_tile_y;

			if(max_x < other_x) {
				max_x = other_x;
			}

			// Ha ha confusion extreme, mixing up those y and z axes for rofls
			if(max_z < other_y) {
				max_z = other_y;
			}
		}
		int[] dim = new int[3];
		dim[0] = max_x;
		dim[1] = max_y;
		dim[2] = max_z;
		return dim;
	}

	/** Get models g3db file name */
	public String getTilesFname() {
		return tiles_g3db;
	}

	/** Check whether file was loaded successfully */
	public boolean fileSuccessfullyLoaded() {
		return successfullyLoaded;
	}

	/** Print the parsed contents of the sir file to string */
	@Override
	public String toString() {
		String s = "SirBinaryFileReader version " + sirBinaryFileReaderVersion + " processed file " + sir_fname + ":\n";

		// Header
		s += "HEADER:\nversion:" + version + "\nnum_floors:" + num_floors + "\nnum_tile_types:" + num_tile_types + "\nnum_prop_types:" + num_prop_types + "\nnum_props:" + num_props + "\n";

		// Files
		s += "FILES:\ntiles_g3db:" + tiles_g3db + "\n";

		// Tile definitions
		s += "TILE DEF:\n";
		for(int i = 0; i < num_tile_types; i++) {
			s += "id:" + tile_def[i].id + " collisionCategory:" + tile_def[i].collisionCategory + " rotation:" + tile_def[i].rotation + "\n";
		}

		// Prop definitions
		s += "PROP DEF:\n";
		for(int i = 0; i < num_prop_types; i++) {
			s += "id:" + prop_def[i].id + " propstate:" + prop_def[i].propstate + " propcoll:" + prop_def[i].propcoll + " ai:" + prop_def[i].ai_algo + "\n";
		}

		// Tiles
		for (int floorIndex = 0; floorIndex < num_floors; floorIndex++) {
			s += "TILES, FLOOR " + floorIndex + ":\n";
			s += "floor_x:" + floors[floorIndex].floor_x + "\n";
			s += "floor_y:" + floors[floorIndex].floor_y + "\n";
			s += "first_tile_x:" + floors[floorIndex].first_tile_x + "\n";
			s += "first_tile_y:" + floors[floorIndex].first_tile_y + "\n";
			for(int y = 0; y < floors[floorIndex].floor_y; y++) {
				for(int x = 0; x < floors[floorIndex].floor_x; x++) {
					s += floors[floorIndex].tiles[x][y] + " ";
				}
			}
			s += "\n";
		}

		// Props
		s += "PROPS:\n";
		for(int i = 0; i < num_props; i++) {
			s += "index:" + props[i].index + " posx:" + props[i].posx + " posy:" + props[i].posy + " posz:" + props[i].posz + " rotation:" + props[i].rotation + "\n";
		}

		// Player
		s += "PLAYER:\nplayer_start_x:" + player_start_x + " player_start_y:" + player_start_y + " player_start_z:" + player_start_z + "\n";

		return s;
	}
}
