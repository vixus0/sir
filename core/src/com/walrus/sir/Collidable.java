package com.walrus.sir;

interface Collidable {

	/**
	 * Add a listener that is notified each time this object collides
	 */
	public void addCollisionListener(CollisionListener listener);

	/**
	 * Called when a collision happens involving this object and collidedWith, causing it to notify all listeners.
	 *
	 */
	public void notifyCollisionListeners(Collidable who, Collidable collidedWith);

	/**
	 * Returns the PhysObject associated with this Collidable.
	 */
	public PhysObject getPhysObject();

	/**
	 * Returns the CollisionCategory associated with this Collidable.
	 */
	public LevelBuilder.CollisionCategory getCollisionCategory();

	/**
	 * Returns the Actor associated with this Collidable.
	 */
	public Actor getActor();
}
