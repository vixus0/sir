package com.walrus.sir;
import com.badlogic.gdx.math.Vector3;

public class AIWaiter implements AI {

	private Actor actor;

	public AIWaiter(Actor actor) {
		this.actor = actor;
		this.actor.disableInstanceRotation();
	}

	public void updateAI() {
		Vector3 playa = ScrGame.sirLevel.player.getPos();
		Vector3 jc = actor.physObj.getPos();
		Vector3 dir = playa.sub(jc).nor();
		this.move(dir);
	}

	private void move(Vector3 dir) {
		this.actor.playAnimation("Armature|run", 10f, 3);

		final float max_speed = 8;
		actor.physObj.body.activate(true);
		dir.scl(max_speed);
		actor.physObj.body.setLinearVelocity(dir);

		// Why is the direction so fucked up?
		final Vector3 dirfuck = new Vector3(dir.z, 0.0f, dir.x);
		this.actor.physObj.faceDirection(dirfuck);
	}
}
