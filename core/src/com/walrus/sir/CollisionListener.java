package com.walrus.sir;

interface CollisionListener {

	/**
	 * Collidable objects will call this function when a collision happens (when they are being listened to),
	 * specifying who they are and who they collided with.
	 */
	public void collision(Collidable who, Collidable collidedWith);
}
