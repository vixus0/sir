package com.walrus.sir;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.collision.btSphereShape;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;

/**
 * Error message display screen.
 */
public class ScrError extends SirScreen {

	private Label errorLabel;

  public ScrError(Sir sir) { super(sir); }

	/**
	 * Build the error screen.
	 */
  @Override
	public void prepare() {
		Table rootTable = new Table();
		rootTable.setFillParent(true);

		Table table = new Table(skin);

		TextButton newButton;

		// Label error message
		errorLabel = new Label("No error message", skin);
		errorLabel.setWrap(true);
		errorLabel.setAlignment(Align.left);
		table.add(errorLabel).width(320).row();

		// Add a button that goes back to start menu
		newButton = new TextButton("I understand, thank you.", skin);
		newButton.addListener(new InputListener() {
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				Gdx.app.log("Sir!", "ScreenError - User has understood and expressed adequate gratitude.");
				game.setUI(Sir.UI.start);
				return true;
			}
		});
		table.add(newButton).expandX();

		table.align(Align.left);

		rootTable.add(table);
		stage.addActor(rootTable);
	}

  @Override
	protected void update() {
		errorLabel.setText(game.error_message);
		errorLabel.invalidate();
	}
}
