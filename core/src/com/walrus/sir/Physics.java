package com.walrus.sir;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.Bullet;
import com.badlogic.gdx.physics.bullet.collision.btBroadphaseInterface;
import com.badlogic.gdx.physics.bullet.collision.btCollisionConfiguration;
import com.badlogic.gdx.physics.bullet.collision.btCollisionDispatcher;
import com.badlogic.gdx.physics.bullet.collision.btDbvtBroadphase;
import com.badlogic.gdx.physics.bullet.collision.btDefaultCollisionConfiguration;
import com.badlogic.gdx.physics.bullet.collision.btDispatcher;
import com.badlogic.gdx.physics.bullet.collision.ContactListener;
import com.badlogic.gdx.physics.bullet.dynamics.btConstraintSolver;
import com.badlogic.gdx.physics.bullet.dynamics.btDiscreteDynamicsWorld;
import com.badlogic.gdx.physics.bullet.dynamics.btDynamicsWorld;
import com.badlogic.gdx.physics.bullet.dynamics.btRigidBody;
import com.badlogic.gdx.physics.bullet.dynamics.btSequentialImpulseConstraintSolver;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.physics.bullet.collision.btCollisionObject;
import com.badlogic.gdx.physics.bullet.collision.ClosestRayResultCallback;

import com.badlogic.gdx.physics.bullet.DebugDrawer;
import com.badlogic.gdx.physics.bullet.linearmath.btIDebugDraw;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.math.collision.Ray;

/**
 * Handles all the bullet physics stuff.
 */
public class Physics implements Disposable {

	// Constants
	public static final float gravity = -10f;
	public static final float friction = 10f;
	public static final float timeStep = 1/60f;
	public static final int subSteps = 5;

	// Collision mask groups
	public static final short COL_NOTHING = 0;
	public static final short COL_RAYCASTABLE = 1; // Why? ;_; Fucking raycast NEEDS to have collision group 1 (no fucking documentation on this ANYWHERE, took me an hour of guesswork)
	public static final short COL_TILE = 1<<1;
	public static final short COL_PROP = 1<<2;
	public static final short COL_PLAYER = 1<<3;

	// Collision mask combos
	public static final short COL_ALL_EXCEPT_RAYCAST = COL_TILE | COL_PROP | COL_PLAYER;
	public static final short COL_ALL = COL_TILE | COL_PROP | COL_PLAYER | COL_RAYCASTABLE;
	public static final short COL_DYNAMIC = COL_PROP | COL_PLAYER;
	public static final short COL_DYNAMIC_AND_RAYCASTABLE = COL_DYNAMIC | COL_RAYCASTABLE;

	// Up vector
	public static final Vector3 UP = new Vector3(0f, 1f, 0f);

	// Ray tracing bs
	private Vector3 rayFrom = new Vector3();
	private Vector3 rayTo = new Vector3();
	private ClosestRayResultCallback rayTester;

	private Array<Collidable> colliders;
	private btDynamicsWorld dynamicsWorld;
	private SirContactListener listener;

	private final btCollisionConfiguration collisionConfig;
	private final btDispatcher dispatcher;
	private final btBroadphaseInterface broadphase;
	private final btConstraintSolver constraintSolver;

	private DebugDrawer debugDrawer;

	// Contact listener
	private class SirContactListener extends ContactListener {
		@Override
		public boolean onContactAdded(int userValue0, int partId0, int index0, boolean match0, int userValue1, int partId1, int index1, boolean match1) {
			// Get the two collidable objects based on their indices in the colliders array
			Collidable obj0 = colliders.get(userValue0);
			Collidable obj1 = colliders.get(userValue1);

			// Call collision handler for those objects whose collision filter
			// matched the collision group (avoiding callbacks on objects which never
			// requested one).
			if(match0) {
				obj0.notifyCollisionListeners(obj0, obj1);
			}
			if(match1) {
				obj1.notifyCollisionListeners(obj1, obj0);
			}

			return true;
		}
	}

	public Physics() {
		Bullet.init();

		collisionConfig = new btDefaultCollisionConfiguration();
		dispatcher = new btCollisionDispatcher(collisionConfig);
		broadphase = new btDbvtBroadphase();
		constraintSolver = new btSequentialImpulseConstraintSolver();

		dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher, broadphase, constraintSolver, collisionConfig);
		dynamicsWorld.setGravity(new Vector3(0, gravity, 0));

		colliders = new Array<Collidable>();
		listener = new SirContactListener();

		rayTester = new ClosestRayResultCallback(rayFrom, rayTo);
	}

	public void addCollidable(Collidable collidable, short collision_group, short collides_with, short callback_filter) {
		final btRigidBody body = collidable.getPhysObject().body;

		body.setContactCallbackFlag(collision_group);
		body.setContactCallbackFilter(callback_filter);

		// If we want to generate contact callbacks for this Actor, set the appropriate collision flag
		if(callback_filter != COL_NOTHING) {
			body.setCollisionFlags(body.getCollisionFlags() | btCollisionObject.CollisionFlags.CF_CUSTOM_MATERIAL_CALLBACK);
		}

		body.setUserValue(colliders.size);
		colliders.add(collidable);
		dynamicsWorld.addRigidBody(body, collision_group, collides_with);
	}

	public void removeCollidable(Collidable collidable) {
		final btRigidBody body = collidable.getPhysObject().body;
		colliders.removeValue(collidable, true);
		dynamicsWorld.removeRigidBody(body);
		recalcCollidersUserValues(); // Yes, this needs to happen... or else.
	}

	/**
	 * This function uses a ray cast to work out what type of tile the prop is sitting on, if any.
	 * For performance reasons, this function IS NOT called by default. In principle it is to be
	 * called by, for example, an AI controller or a Player controller class. Even then, it should
	 * only be called if and when the result is actually needed. For example, when the player wants
	 * to jump, it is necessary to check that he is standing on FLOOR or STAIRS. Otherwise, do not
	 * call this function.
	 */
	public LevelBuilder.CollisionCategory rayCasterProXL(Vector3 A, Vector3 B) {
		rayFrom.set(A);
		rayTo.set(B);
		rayTester.setCollisionObject(null);
		rayTester.setClosestHitFraction(1f);
		rayTester.setRayFromWorld(rayFrom);
		rayTester.setRayToWorld(rayTo);

		dynamicsWorld.rayTest(rayFrom, rayTo, rayTester);

		// Return the closest raycastable object the ray intersected
		if (rayTester.hasHit()) {
			final int userValue0 = rayTester.getCollisionObject().getUserValue();
			final Collidable obj0 = colliders.get(userValue0);
			return obj0.getCollisionCategory();
		} else {
			// No hits
			return LevelBuilder.CollisionCategory.nothing;
		}
	}

	public Collidable rayCasterQuadPlusXtreme(Ray ray) {
		rayFrom.set(ray.origin);
		rayTo.set(ray.direction).scl(50f).add(rayFrom);
		rayTester.setCollisionObject(null);
		rayTester.setClosestHitFraction(1f);
		rayTester.setRayFromWorld(rayFrom);
		rayTester.setRayToWorld(rayTo);

		dynamicsWorld.rayTest(rayFrom, rayTo, rayTester);

		// Return the closest raycastable object the ray intersected
		if (rayTester.hasHit()) {
			final int userValue0 = rayTester.getCollisionObject().getUserValue();
			final Collidable obj0 = colliders.get(userValue0);
			return obj0;
		} else {
			// No hits
			return null;
		}
	}

	public void update(float dt) {
		final float delta = Math.min(timeStep*2f, dt);
		dynamicsWorld.stepSimulation(delta, subSteps, timeStep);
	}

	@Override
	public void dispose() {
		dynamicsWorld.dispose();
		listener.dispose();

		if (rayTester != null) {
			rayTester.dispose();
		}
		rayTester = null;
	}

	public void initDebugDrawer() {
		debugDrawer = new DebugDrawer();
		debugDrawer.setDebugMode(btIDebugDraw.DebugDrawModes.DBG_MAX_DEBUG_DRAW_MODE);
		dynamicsWorld.setDebugDrawer(debugDrawer);
	}

	public void drawDebug(PerspectiveCamera camera) {
		debugDrawer.begin(camera);
		dynamicsWorld.debugDrawWorld();
		debugDrawer.end();
	}

	/**
 	* If any colliders are removed from the list, all the uservalues of the corresponding collision bodies
 	* have to updated, because otherwise IT'S NULL POINTER EXCEPTIONS ALL THE WAY DOWN BRO ALL THE WAY DOWN
 	*/
	private void recalcCollidersUserValues() {
		for(int i = 0; i < colliders.size; i++) {
			colliders.get(i).getActor().physObj.body.setUserValue(i);
		}
	}
}
