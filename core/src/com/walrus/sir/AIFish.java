package com.walrus.sir;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.Gdx;

public class AIFish implements AI {

	private Actor actor;

	private boolean isInWater;
	private int hopCount;
	private final int hopInterval = 20;

	public AIFish(Actor actor) {
		this.actor = actor;
		this.isInWater = false;
		hopCount = 0;
	}

	public void updateAI() {
		// If not in water, thrash around like mad
		if (isInWater == false) {
			this.actor.playAnimation("Armature|thrash", 3f, 3);
			if(hopCount > hopInterval) {
				hopCount = 0;
		        final float jump_impulse = 80;
		        actor.physObj.body.activate(true);
		        actor.physObj.body.applyCentralImpulse(new Vector3((-0.5f + ScrGame.rng.nextFloat()) * jump_impulse*.5f, jump_impulse, (-0.5f + ScrGame.rng.nextFloat()) * jump_impulse*.5f));
			} else {
				hopCount += 1;
			}
		}
	}
}
