package com.walrus.sir;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.collision.btSphereShape;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.Screen;

/**
 * Abstract class managing a scene2d stage of widgets making up a UI menu and screen. This class can optionally be extended to override
 * the update() function, which is called every draw cycle.
 */
public abstract class SirScreen implements Screen {

  protected final Stage stage;
  protected final Sir game;
  protected final Skin skin;

  protected Sir.UI queued_ui;

  /**
   * Create the stage - the full screen scene2d handler master atrocity in which everything takes place
   */
  public SirScreen(Sir sir) {
    stage = new Stage();
    game = sir;
		skin = Sir.assets.get("uiskin.json");
    skin.add("logo", Sir.assets.get("sirlogo.png"));
  }

  @Override
  public void dispose() {
    Gdx.app.log("Sir!", "SirScreen - Dispose");
    stage.dispose();
  }

  /**
   * Update and draw this screen
   */
  @Override
  public void render(float delta) {
    clearScreen();
    update();
    stage.act();
    stage.draw();
  }

  @Override
  public void resize(int width, int height) {
    Gdx.app.log("Sir!", "SirScreen - Resize - " + width + " x " + height);
    stage.getViewport().update(width, height, true);
  }

  @Override
  public void resume() {
    Gdx.app.log("Sir!", "SirScreen - Resume");
    handleInputs();
  }

  @Override
  public void show() {
    Gdx.app.log("Sir!", "SirScreen - Show");
    handleInputs();
  }

  @Override
  public void hide() {
    Gdx.app.log("Sir!", "SirScreen - Hide");
  }

  @Override
  public void pause() {
    Gdx.app.log("Sir!", "SirScreen - Pause");
  }

  private void clearScreen() {
    Gdx.graphics.getGL20().glClearColor( 0, 0, 0, 1 );
    Gdx.graphics.getGL20().glClear( GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT );
  }

  /**
   * Queues the next UI screen.
   */
  public void queueUI(Sir.UI ui) {
    queued_ui = ui;
  }

  /**
   * Switch to next UI screen.
   */
  public void nextUI() {
    game.setUI(queued_ui);
  }

  /**
   * Adds this object's controllers to the global input multiplexer
   */
  public void handleInputs() {
    Sir.inputs.addProcessor(stage);
  }

  /**
   * A method which can optionally be overridden to provide behaviour every time the UI is redrawn. This class' render() method calls it.
   */
  protected void update() {}

  /**
   * This method is called whenever Sir.setUI() is used. It is the first method called even before the screen is switched to this one.
   * Override to provide functionality when switched to.
   */
  public void prepare() {}
}
