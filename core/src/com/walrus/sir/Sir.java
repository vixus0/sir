package com.walrus.sir;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.InputMultiplexer;

import java.util.EnumMap;

public class Sir extends Game {

	/** List of SirScreen objects, referenced by the enums listed in UI */
	public static enum UI { error, start, choose_level, loading, game }
	public static EnumMap<Sir.UI, SirScreen> screens;

	/** Global asset loader */
	public static AssetManager assets;

  /** The horror... THE HORROR */
  public static InputMultiplexer inputs;

	/** String which is displayed by the error screen. Can be set from any part of game. */
	public static String error_message;

	/**
	 * Replacement for the AssetManager progress indicator, which gives no indication of how far
	 * through loading we are. This var is updated from the SirLevelLoader itself.
	 */
	public static float levelLoadingProgress;

	@Override
	public void create () {
		Gdx.app.log("Sir!", "Let's get this shiz on, muthafuqqaz");

		// Prepare for extreme loading
    assets = new AssetManager();

		// Register the Sir Level file custom loader
		assets.setLoader(SirLevel.class, new SirLevelLoader());

		// Create and set the global input handler
		inputs = new InputMultiplexer();
		Gdx.input.setInputProcessor(inputs);

		// Load all the assets and set up the styles used by our UI
    assets.load("uiskin.json", Skin.class);
    assets.load("sirlogo.png", Texture.class);
    assets.finishLoading();

		// Build UI
		screens = new EnumMap<Sir.UI, SirScreen>(Sir.UI.class);
		screens.put(Sir.UI.error, new ScrError(this));
		screens.put(Sir.UI.start, new ScrStart(this));
		screens.put(Sir.UI.choose_level, new ScrChooseLevel(this));
		screens.put(Sir.UI.loading, new ScrLoading(this));
		screens.put(Sir.UI.game, new ScrGame(this));

    // Make sure ScrLoading loads ScrGame next
    screens.get(Sir.UI.loading).queueUI(Sir.UI.game);

		// Set off from the start menu
		setUI(Sir.UI.start);
	}

	/**
	 * Sets app focus to the SirScreen referenced by the given UI enum.
	 */
	public void setUI(Sir.UI ui) {
    Sir.inputs.clear(); // Get rid of current input processors
    SirScreen new_screen = screens.get(ui);
    new_screen.prepare();
    super.setScreen(new_screen);
	}

	@Override
	public void dispose() {
		assets.dispose();

		for (Sir.UI ui : Sir.UI.values()) {
			screens.get(ui).dispose();
		}
	}
}
