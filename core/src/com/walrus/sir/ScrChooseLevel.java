package com.walrus.sir;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.collision.btSphereShape;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

//import java.io.File;
//import java.io.FilenameFilter;

/**
 * The level loader menu, allowing user to choose from list of installed levels.
 */
public class ScrChooseLevel extends SirScreen {

  private List<String> levelList;

  public ScrChooseLevel(Sir sir) { super(sir); }

  @Override
  public void prepare() {
    // The love monster
    //Image love = new Image(skin, "chooselevel");
    //stage.addActor(love);

    // Get a list of all the .sir files in the levels folder

	FileHandle dir;
	if(Gdx.app.getType() == ApplicationType.Android) {
		dir = Gdx.files.internal("levels/");
	} else {
		// ApplicationType.Desktop ..
		dir = Gdx.files.internal("levels/");
	}

    Array<String> sirFnames = new Array<String>();
	for (FileHandle fh: dir.list()) {
		sirFnames.add(fh.name());
	}

	if(sirFnames.size == 0) {
		Sir.error_message = "No levels found.";
		game.setUI(Sir.UI.error);
	}

    // Convert all the sir files to buttons that load that file
    levelList = new List<String>(skin);
    levelList.setItems(sirFnames);
    levelList.getSelection().setMultiple(true);
    levelList.addListener( new InputListener() {
        @Override
        public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        // Begin loading of the selected level
        ScrGame.levelFname = "levels/" + levelList.getSelected();
        Gdx.app.log("Sir!", "MenuChooseLevel - " + ScrGame.levelFname);

        if(SirEditSelector.edit == false) {
        	Sir.assets.load(ScrGame.levelFname, SirLevel.class);
        }

        // Switch to the loading splash screen
        game.setUI(Sir.UI.loading);
          return true;
        }
    });

	Table rootTable = new Table();
	rootTable.setFillParent(true);
	Table tableL = new Table(skin);
    ScrollPane scrollPane = new ScrollPane(levelList, skin);
	tableL.add(scrollPane).expandX();
	rootTable.add(tableL);
	stage.addActor(rootTable);
  }
}
