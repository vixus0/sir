package com.walrus.sir;

import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.Gdx;

public class ChaseCamera {
	public static final float fov = 67f;

	private final PerspectiveCamera camera;

	private Actor target;
	private Vector3 relativePos;
	private Vector3 currentPos;
	private float stiffness;
	private boolean canRotate;

	public ChaseCamera(float w, float h) {
		camera = new PerspectiveCamera(fov, w, h);
		camera.position.set(-10f, 30f, -10f);
		camera.near = 0.1f;
		camera.far  = 300f;
		camera.update();
	}

	public void chaseThisObject(Actor target, float relativePosX, float relativePosY, float relativePosZ, float stiffness, boolean canRotate) {
		this.target = target;
		this.stiffness = stiffness;
		this.canRotate = canRotate;
		this.relativePos = new Vector3(relativePosX, relativePosY, relativePosZ);
		this.currentPos = new Vector3(target.physObj.getPos());

		if(canRotate == false) {
			camera.position.set(currentPos);
			Vector3 targetPos = new Vector3(currentPos);
			targetPos.sub(relativePos);
			camera.lookAt(targetPos);
		}
	}

	public void update(float dt) {
		if(target == null) {
			Gdx.app.log("Sir!", "ChaseCamera: Chase target is unset (null)");
			return;
		}

		// Get current target position
		final Vector3 targetPos = target.physObj.getPos();

		// Get the vector to move camera to target position
		final Vector3 move = new Vector3(targetPos);
		currentPos.add(move.add(relativePos).sub(currentPos).scl(dt * stiffness));

		// Update camera transforms
		camera.position.set(currentPos);

		if(canRotate==true) {
			camera.lookAt(targetPos);
		}
		camera.up.set(Vector3.Y);
		camera.update();
	}

	public PerspectiveCamera get_camera() {
		return camera;
	}
}
