package com.walrus.sir;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.collision.btSphereShape;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

/**
 * The start menu, allowing user to choose whether to play a new game, load level, etc.
 */
public class ScrStart extends SirScreen {

  public ScrStart(Sir sir) { super(sir); }

	@Override
	public void prepare() {
		SirEditSelector.edit = false;

		Table rootTable = new Table();
		rootTable.setFillParent(true);
		Table tableL = new Table(skin);


		TextButton newButton;

		// Add a button that loads a level and initiates play
		newButton = new TextButton("New game", skin);
		newButton.addListener(new InputListener() {
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				Gdx.app.log("Sir!", "ScrStart - New game");
				SirEditSelector.edit = false;
				game.setUI(Sir.UI.choose_level);
				return true;
			}
		});
		tableL.add(newButton).width(150).row();

		// Add a button that loads the editor with an empty level
		newButton = new TextButton("Editor: New Level", skin);
		newButton.addListener(new InputListener() {
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				Gdx.app.log("Sir!", "ScrStart - New Level (Editor)");
				new SirEditSelector();
				game.setUI(Sir.UI.loading);
				return true;
			}
		});
		tableL.add(newButton).width(150).row();

		// Add a button that loads the editor with a loaded level
		newButton = new TextButton("Editor: Load Level", skin);
		newButton.addListener(new InputListener() {
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				Gdx.app.log("Sir!", "ScrStart - Load Level (Editor)");
				new SirEditSelector();
				game.setUI(Sir.UI.choose_level);
				return true;
			}
		});
		tableL.add(newButton).width(150).row();

		CheckBox debugCheck = new CheckBox("Debug Draw Mode", skin);
		debugCheck.addListener(new InputListener() {
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				if(ScrGame.debugDrawMode == false) {
					ScrGame.debugDrawMode = true;
				} else {
					ScrGame.debugDrawMode = false;
				}

				Gdx.app.log("Sir!", "ScrStart - Debug draw mode - " + ScrGame.debugDrawMode);

				return true;
			}
		});
		tableL.add(debugCheck).width(150).row();

		// Add a button that exits
		newButton = new TextButton("Exit", skin);
		newButton.addListener(new InputListener() {
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				Gdx.app.log("Sir!", "ScrStart - Exit");
				Gdx.app.exit();
				return true;
			}
		});
		tableL.add(newButton).width(150).row();


		Table tableR = new Table(skin);
		Image logo = new Image(skin, "logo");
		tableR.add(logo).expandX().row();

		rootTable.add(tableL);
		rootTable.add(tableR);
		stage.addActor(rootTable);
	}
}
