package com.walrus.sir;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.collision.btSphereShape;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener.ChangeEvent;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

/**
 * The asset loading screen. Once all assets are loaded, it switches to the queued UI.
 */
public class ScrLoading extends SirScreen {

	private ProgressBar progressBar;
	private Label loadingLabel;
	private boolean trigger;
	private final float smooth = .5f;

  public ScrLoading(Sir sir) { super(sir); }

	/**
	 * The loading menu must be told what menu to switch to once assets have finished loading
	 */
	@Override
	public void prepare() {
        Table rootTable = new Table();
		Table table = new Table(skin);
        rootTable.setFillParent(true);

		// Labels
		loadingLabel = new Label("", skin);
		table.add(loadingLabel).expandX().row();

		// Progress bar
		progressBar = new ProgressBar(0f, 1f, 0.1f, false, skin);
		progressBar.setSize(Gdx.graphics.getWidth(), progressBar.getPrefHeight());
		progressBar.setAnimateDuration(smooth);
		table.add(progressBar).width(Gdx.graphics.getWidth());

		rootTable.add(table);
        stage.addActor(rootTable);

		trigger = true;
	}

	@Override
	protected void update() {
		final float progress;

		// If we're loading a level, use the better progress estimator,
		// otherwise use the usual AssetManager getProgress()
		if(SirEditSelector.edit == false) {
			progress = Sir.levelLoadingProgress;
		} else {
			progress = Sir.assets.getProgress();
		}

		loadingLabel.setText("Loading... " + String.format("%.0f%%", progress*100));
		progressBar.setValue(progress);
		try {
			if(Sir.assets.update() && trigger == true) {
				Gdx.app.log("Sir!", "ScrLoading - Level loading complete");

				 // For purely cosmetic reasons ;)
				Timer.schedule(new Timer.Task(){
					@Override public void run() {
						trigger = true;
            nextUI();
					}
				}, smooth);

				// Don't trigger the timer schedule a billion times
				trigger = false;
			}
		} catch(GdxRuntimeException e) {
			Sir.error_message = e.getMessage();
			game.setUI(Sir.UI.error);
		}
	}
}
