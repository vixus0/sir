package com.walrus.sir;

import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.utils.AnimationController;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.physics.bullet.collision.btCollisionShape;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.graphics.g3d.model.Animation;
import com.badlogic.gdx.math.Matrix4;

/**
 * Represents a prop or character in the game.
 * Provides a link between the visible animated model
 * and corresponding physics object in Bullet.
 */
class Actor implements Collidable, Disposable {

	public final String name;
	public final ModelInstance instance;
	public final AnimationController animator;
	public PhysObject physObj;
	private final Array<CollisionListener> collisionListeners;

	public final BoundingBox bbox;
	public final Vector3 center;
	public final float radius;

	private final LevelBuilder.CollisionCategory collisionCategory;

	/**
	 * Create a new Actor from the given model, creating the physObj from collision data in the model.
	 */
	public Actor(String name, Model model, Vector3 pos, Vector3 rot, LevelBuilder.PropState propstate, LevelBuilder.CollisionCategory collisionCategory) {
		this(name, model, null, pos, rot, collisionCategory, true);

		physObj = new PhysObject(instance, propstate);
		physObj.body.proceedToTransform(instance.transform);
	}

	/**
	 * Create a new Actor with the given model, using the given collision shape
	 */
	public Actor(String name, Model model, Vector3 pos, Vector3 rot, btCollisionShape colShape, float mass, LevelBuilder.CollisionCategory collisionCategory) {
		this(name, model, null, pos, rot, collisionCategory, true);

		physObj = new PhysObject(instance, colShape, mass);
		physObj.body.proceedToTransform(instance.transform);
	}

	/**
	 * Create a new Actor with nodeId of the given model, using the given collision shape
	 */
	public Actor(String name, Model model, String nodeId, Vector3 pos, Vector3 rot, btCollisionShape colShape, float mass, LevelBuilder.CollisionCategory collisionCategory) {
		this(name, model, nodeId, pos, rot, collisionCategory, true);

		physObj = new PhysObject(instance, colShape, mass);
		physObj.body.proceedToTransform(instance.transform);
	}

	/**
	 * Private constructor. Use one of the other three.
	 */
	private Actor(String name, Model model, String nodeId, Vector3 pos, Vector3 rot, LevelBuilder.CollisionCategory collisionCategory, boolean paulTulip) {
		this.name = name;
		if(nodeId == null) {
			instance = new ModelInstance(model);
		} else {
			instance = new ModelInstance(model, nodeId, true);
		}
		animator = new AnimationController(instance);

		int numAnim = 0;
		for (Animation anim : instance.animations) {
			numAnim += 1;
			Gdx.app.log("Sir!", "Animation found for Actor '" + this.name + " " + anim.id);
		}

		if(numAnim == 0) {
//			Gdx.app.log("Sir!", "No animations found for prop " + this.name);
		}

		// Set model rotation
    Matrix4 rotMat = new Matrix4().setFromEulerAngles(rot.x, rot.y, rot.z);
		instance.transform.mul(rotMat);
		instance.transform.trn(pos); // This *must* come after the rotation matrix creation

		// Prepare object for addition of collision listeners
		collisionListeners = new Array<CollisionListener>();

		// For frustum culling
		Vector3 dims = new Vector3();
		bbox = new BoundingBox();
		center = new Vector3();
		instance.calculateBoundingBox(bbox);
		bbox.getCenter(center);
		bbox.getDimensions(dims);
		radius = dims.len();

		// Remember what category this prop is in case someone ray casts it
		this.collisionCategory = collisionCategory;
	}

	@Override
	public void notifyCollisionListeners(Collidable who, Collidable collidedWith) {
		for(CollisionListener listener : collisionListeners) {
			listener.collision(this, collidedWith);
		}
	}

	@Override
	public void addCollisionListener(CollisionListener listener) {
		collisionListeners.add(listener);
	}

	@Override
	public PhysObject getPhysObject() {
		return physObj;
	}

	@Override
	public LevelBuilder.CollisionCategory getCollisionCategory() {
		return collisionCategory;
	}


	public void updateAnimationController(float delta) {
		animator.update(delta);
	}

	public void playAnimation(String actionName, float speed, int repetitions) {
		try {
			animator.setAnimation(actionName, repetitions, speed, null);
		} catch(GdxRuntimeException e) {
      // Animation missing, so nothing to do.
		}
	}

	public void playAction(String actionName, float speed, int repetitions, float transitionTime) {
		try {
			animator.action(actionName, repetitions, speed, null, transitionTime);
		} catch(GdxRuntimeException e) {
      // Animation missing, so nothing to do.
		}
	}

	public void disableInstanceRotation() {
		physObj.disableInstanceRotation();
	}

	@Override
	public Actor getActor() {
		return this;
	}

	@Override
	public void dispose () {
		physObj.dispose();
	}
}
