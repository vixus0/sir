package com.walrus.sir;

public interface AI {

	/** Allows AI object to carry out its AI algorithm for this step */
	public void updateAI();

}
