package com.walrus.sir;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.collision.btSphereShape;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener.ChangeEvent;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Scaling;

import java.util.Random;

/**
 * The asset loading screen. Once all assets are loaded, it opens the UI specified by the nextMenu variable.
 */
public class ScrGame extends SirScreen {

	public static int win_w, win_h;

	public static int points;
	public static float elapsed_time, fps;
  public static ModelBatch mbatch;
  public static SpriteBatch sbatch;
	public static ChaseCamera chase;
	public static SirLevel sirLevel;
	public static BitmapFont font;
	public static Timer timer;
	public static Random rng;
	public static Vector3 trans;
	public static boolean debugDrawMode;

	/** Filename of currently loading/loaded level file */
	public static String levelFname;

	private boolean running;

	public ScrGame(Sir sir) {
    super(sir);

		running = false;

		win_w = Gdx.graphics.getWidth();
		win_h = Gdx.graphics.getHeight();

		chase = new ChaseCamera(win_w, win_h);
		mbatch = new ModelBatch();
		sbatch = new SpriteBatch();
		font = new BitmapFont();
		timer = new Timer();
		rng = new Random();

		trans = new Vector3();

    // Add a button that loads a level and initiates play
    TextButton newButton = new TextButton("Back", skin);
    newButton.setPosition(0, win_h-50);
    newButton.addListener(new InputListener() {
      public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
        Gdx.app.log("Sir!", "ScrGame - Back");
        running = false; // Make sure the ScrGame restarts itself properly next time
        SirEditSelector.selector = null; // Get rid of the current edit selector
        sirLevel.dispose();
        game.setUI(Sir.UI.start);
        return true;
      }
    });
    stage.addActor(newButton);
	}

	private void init(SirLevel sirLevel) {
		// Get the SirLevel loaded with the asset manager
		this.sirLevel = sirLevel;

		// Reset some shiz
		points = 0;
		elapsed_time = 0f;

		// Tell camera to chase player
    chase.chaseThisObject(sirLevel.player.getActor(), 0f, 4f, 4f, 3.5f, true);

		// Give input processing to player
		sirLevel.player.handleInputs();
	}

	/**
	 * This method is called whenever the UI switches to this screen. First it checks if it is already in the midst
	 * of running a game (running == true) in which case it resumes the game by setting the input processor back to the
	 * player (rather than the previous screen's stage). However, if running == false, it will attempt to use whatever
	 * SirLevel object is referenced by levelFname to start a new game.
	 */
	@Override
	public void prepare() {
		// If a level was not already in session, start a new one with the loaded SirLevel object
		if(running == false) {
			if(SirEditSelector.edit == false) {
				Gdx.app.log("Sir!", "ScrGame - Starting new game using fname=" + levelFname);
				final SirLevel newSirLevel = Sir.assets.get(levelFname);
				init(newSirLevel);
				running = true;
			} else {
				Gdx.app.log("Sir!", "ScrGame - Starting level editing");
				this.sirLevel = SirEditSelector.selector.init();
				if(ScrGame.levelFname != null) {
					SirEditSelector.selector.loadOld(ScrGame.levelFname);
				}
				chase.chaseThisObject(SirEditSelector.selector.getActor(), 0f, 5f, 2f, 2f, false);
				SirEditSelector.selector.handleInputs();
				running = true;
			}
		} else {
			// Otherwise, resume play
			Gdx.app.log("Sir!", "ScrGame - Resuming play");

			if(SirEditSelector.edit == false) {
				// Give input processing to player
				sirLevel.player.handleInputs();
			} else {
				// Give input processing to edit selector
				SirEditSelector.selector.handleInputs();
			}
		}
	}

	@Override
	protected void update() {
		// Get timestep
		final float dt = Gdx.graphics.getDeltaTime();
		fps = (float)Math.ceil(1f/dt);

		// Update AIs
		for(AI ai : sirLevel.ais) {
			ai.updateAI();
		}

		// Update the bullet physics world
		sirLevel.physics.update(dt);

		// Move the chase cam
		chase.update(dt);

		// Render everything
		renderAll(dt);
	}

	/**
	* Main rendering function.
	*/
	private void renderAll(float dt) {
		// Set viewport and double buffering
		Gdx.gl.glViewport(0, 0, (int) win_w, (int) win_h);
		Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

		// Render all the 3D stuff
		worldRender(dt);

		// Render all the 2D stuff
		spriteRender(dt);

		if(debugDrawMode == true) {
			sirLevel.physics.drawDebug(chase.get_camera());
		}
	}

	/**
	 * Render all 3D stuff.
	 */
	private void worldRender(float dt) {
		mbatch.begin(chase.get_camera());
		for(Actor a : sirLevel.actors) {
			a.updateAnimationController(dt);
			a.instance.transform.getTranslation(trans);
			trans.add(a.center);
			if(chase.get_camera().frustum.sphereInFrustum(trans, a.radius)) {
				mbatch.render(a.instance, sirLevel.env);
			}
		}
		mbatch.end();
	}

	/**
	 * Render all 2D sprite stuff.
	 */
	private void spriteRender(float dt) {
		sbatch.begin();
		text("FPS: " + fps + " " + SirEditSelector.selector);
		sbatch.end();
	}

	/**
	 * Write some text to the top left corner of the screen.
	 */
	private void text(String str) {
		font.draw(sbatch, str, 20f, 30f, win_w - 12f, Align.left, true);
	}

	/**
	 * Dispose any resources.
	 */
	public void dispose() {
		super.dispose();
		if(sirLevel != null) {
			sirLevel.dispose();
		}
		mbatch.dispose();
		sbatch.dispose();
	}

    @Override
    public void pause () {
		super.pause();
        timer.stop();
    }

    @Override
    public void resume () {
		super.resume();
        timer.start();
    }

    @Override
    public void resize (int width, int height) {
        win_w = width;
        win_h = height;
    }
}
