package com.walrus.sir;
import com.badlogic.gdx.math.Vector3;

public class AICrackdown implements AI {

	private Actor actor;

	public AICrackdown(Actor actor) {
		this.actor = actor;
	}

	public void updateAI() {
		Vector3 playa = ScrGame.sirLevel.player.getPos();
		Vector3 jc = actor.physObj.getPos();
		Vector3 dir = playa.sub(jc).nor();
		this.move(dir);
	}

	private void move(Vector3 dir) {
		final float max_speed = 5;

		actor.physObj.body.activate(true);
		dir.scl(max_speed);
		actor.physObj.body.setLinearVelocity(dir);
	}
}
