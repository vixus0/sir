package com.walrus.sir;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.physics.bullet.collision.btSphereShape;
import com.badlogic.gdx.physics.bullet.collision.btCollisionShape;
import com.badlogic.gdx.physics.bullet.collision.btConvexHullShape;
import com.badlogic.gdx.physics.bullet.collision.btShapeHull;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.utils.BufferUtils;
import com.badlogic.gdx.Input.Keys;
import java.nio.FloatBuffer;
import com.badlogic.gdx.graphics.g3d.model.MeshPart;
import com.badlogic.gdx.math.collision.Ray;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import java.io.*;
/**
 * The SirEditSelector controls an Actor as the selection indicator when editing levels
 */
public class SirEditSelector {

  public static boolean edit = false;
  public static SirEditSelector selector = null;
	private SirLevel sirLevel = null;

	private final String output_txt_fname = "rofl.txt";

	public Actor actorDiamond, actorCube, actorPropSelect, actorKid=null;
	private GestureDetector gestures;
	private InputAdapter keyboardInput;

	private int select_x, select_y, select_z;
	private float diamond_x, diamond_y, diamond_z;
	private float spacing = 2.0f;

	private enum EditMode { tile_mode, prop_mode }
	private EditMode editMode;

	private class PROP_DEF {
		public String name;
		public String id;
		public LevelBuilder.PropState propstate;
		public LevelBuilder.AIAlgo ai_algo;

		public PROP_DEF(String id, LevelBuilder.PropState propstate, LevelBuilder.AIAlgo ai_algo) {
			this.id = id;
			this.propstate = propstate;
			this.ai_algo = ai_algo;

			this.name = id + ";" + propstate + ";" + ai_algo;
		}

		public String toString() {
			return name;
		}
	}
	private Array<PROP_DEF> prop_def;
	private int prop_index = 0;

	private class PropROFL99 {
		public Actor actor;
		public int prop_index;
		public PropROFL99(Actor actor, int prop_index) {
			this.actor = actor;
			this.prop_index = prop_index;
		}
	};
	private Array<PropROFL99> added_props;

    private class TILE_DEF {
    	public String txt_char;
    	public String name;
    	public String id;
    	public LevelBuilder.CollisionCategory collisionCategory;
    	public short rotation;

    	public TILE_DEF(String txt_char, String name, String id, LevelBuilder.CollisionCategory collisionCategory, short rotation) {
	      	this.txt_char = txt_char;
    	  	this.name = name;
      		this.id = id;
	      	this.collisionCategory = collisionCategory;
    	  	this.rotation = rotation;
    	}

	    public String getTileDefString() {
	      return this.txt_char + " " + this.id + " " + this.collisionCategory + " " + this.rotation;
	    }
	}

    private final TILE_DEF[] tile_def = {
		new TILE_DEF(".", "Floor", "floor", LevelBuilder.CollisionCategory.floor, (short)0),
		new TILE_DEF("N", "Wall N", "wall", LevelBuilder.CollisionCategory.wall, (short)180),
		new TILE_DEF("E", "Wall E", "wall", LevelBuilder.CollisionCategory.wall, (short)90),
		new TILE_DEF("S", "Wall S", "wall", LevelBuilder.CollisionCategory.wall, (short)0),
		new TILE_DEF("W", "Wall W", "wall", LevelBuilder.CollisionCategory.wall, (short)270),
		new TILE_DEF("1", "Corner SE", "corner", LevelBuilder.CollisionCategory.wall, (short)0),
		new TILE_DEF("2", "Corner NE", "corner", LevelBuilder.CollisionCategory.wall, (short)90),
		new TILE_DEF("3", "Corner NW", "corner", LevelBuilder.CollisionCategory.wall, (short)180),
		new TILE_DEF("4", "Corner SW", "corner", LevelBuilder.CollisionCategory.wall, (short)270),
		new TILE_DEF("X", "Darktile", "darktile", LevelBuilder.CollisionCategory.wall, (short)0),
		new TILE_DEF("-", "Stairs N", "stairs", LevelBuilder.CollisionCategory.stairs, (short)0),
		new TILE_DEF("_", "Stairs S", "stairs", LevelBuilder.CollisionCategory.stairs, (short)180),
		new TILE_DEF(">", "Stairs E", "stairs", LevelBuilder.CollisionCategory.stairs, (short)270),
		new TILE_DEF("<", "Stairs W", "stairs", LevelBuilder.CollisionCategory.stairs, (short)90)
	};

	private int tile_index = 0;

	private final int size_x = 50, size_y = 10, size_z = 50;
	private int offset_x = 0, offset_z = 0;

	private class TileROFL99 {
		public Actor actor;
		public int tile_index;
		public TileROFL99(Actor actor, int tile_index) {
			this.actor = actor;
			this.tile_index = tile_index;
		}
	};
	private TileROFL99[][][] tiles;

	int button_pressed = -1;

	int hovered_over_prop_index = -1;

	public SirEditSelector() {
		Gdx.app.log("Sir!", "SirEditSelector - Constructor");
		selector = this;
		edit = true;
		this.loadAssets();
	}

	/** Load all possible assets the editor may wish to use */
	public void loadAssets() {
		// Load kid
		Sir.assets.load("player/kid_game.g3db", Model.class);

		// Load tiles
		Sir.assets.load("tiles/restaurant.g3db", Model.class);

		// Load props
		prop_def = new Array<PROP_DEF>();
		prop_def.add(new PROP_DEF("candlehold", LevelBuilder.PropState.dynamicprop, LevelBuilder.AIAlgo.none));
		prop_def.add(new PROP_DEF("champagne", LevelBuilder.PropState.dynamicprop, LevelBuilder.AIAlgo.none));
		prop_def.add(new PROP_DEF("clock", LevelBuilder.PropState.dynamicprop, LevelBuilder.AIAlgo.none));
		prop_def.add(new PROP_DEF("drumstick", LevelBuilder.PropState.dynamicprop, LevelBuilder.AIAlgo.none));
		prop_def.add(new PROP_DEF("fish", LevelBuilder.PropState.dynamicprop, LevelBuilder.AIAlgo.none));
		prop_def.add(new PROP_DEF("moustache", LevelBuilder.PropState.dynamicprop, LevelBuilder.AIAlgo.none));
		prop_def.add(new PROP_DEF("paintingXL", LevelBuilder.PropState.dynamicprop, LevelBuilder.AIAlgo.none));
		prop_def.add(new PROP_DEF("plateL", LevelBuilder.PropState.dynamicprop, LevelBuilder.AIAlgo.none));
		prop_def.add(new PROP_DEF("plateS", LevelBuilder.PropState.dynamicprop, LevelBuilder.AIAlgo.none));
		prop_def.add(new PROP_DEF("potplant", LevelBuilder.PropState.dynamicprop, LevelBuilder.AIAlgo.none));
		prop_def.add(new PROP_DEF("table", LevelBuilder.PropState.dynamicprop, LevelBuilder.AIAlgo.none));
		prop_def.add(new PROP_DEF("trolley", LevelBuilder.PropState.dynamicprop, LevelBuilder.AIAlgo.none));
		prop_def.add(new PROP_DEF("winebottle", LevelBuilder.PropState.dynamicprop, LevelBuilder.AIAlgo.none));

		for(int i = 0; i < prop_def.size; i++) {
			Sir.assets.load("props/" + prop_def.get(i).id + ".g3db", Model.class);
		}

		// Load the selector diamond and cube
		Sir.assets.load("player/selectorcube.g3db", Model.class);
		Sir.assets.load("player/selectordiamond.g3db", Model.class);
		Sir.assets.load("player/selectorprop.g3db", Model.class);
	}


	public SirLevel init() {

		sirLevel = new SirLevel();
		sirLevel.physics.initDebugDrawer();
        sirLevel.env.set(new ColorAttribute(ColorAttribute.AmbientLight, 1.0f, 1.0f, 1.0f, 1f)); // Don't forget the lights...

		// Start off in tile edit mode
		editMode = EditMode.tile_mode;

		// Allocate tile array and initialise it to empty (-1)
		tiles = new TileROFL99[size_x][size_y][size_z];
		for(int x = 0; x < size_x; x++) {
			for(int y = 0; y < size_y; y++) {
				for(int z = 0; z < size_z; z++) {
					tiles[x][y][z] = new TileROFL99(null, -1);
				}
			}
		}

		added_props = new Array<PropROFL99>();

		// Create smooth moving selector diamond (camera follows this selector)
		Model selectorModel = Sir.assets.get("player/selectordiamond.g3db");
		Actor selectorActor = new Actor("selectorDiamond", selectorModel, new Vector3(0f,0f,0f), new Vector3(0f, 0f, 0f), new btSphereShape(.1f), 10f, LevelBuilder.CollisionCategory.nothing);
		sirLevel.instances.add(selectorActor.instance);
		sirLevel.actors.add(selectorActor);

		// Create a green selector cube to indicate which tile is currently selected
		Model selectorcubeModel = Sir.assets.get("player/selectorcube.g3db");
		Actor selectorcubeActor = new Actor("selectorCube", selectorcubeModel, new Vector3(0f,0f,0f), new Vector3(0f, 0f, 0f), new btSphereShape(.1f), 10f, LevelBuilder.CollisionCategory.nothing);
		sirLevel.instances.add(selectorcubeActor.instance);
		sirLevel.actors.add(selectorcubeActor);

		// Create a red selector to indicate which tile is currently selected
		Model selectorpropModel = Sir.assets.get("player/selectorprop.g3db");
		Actor selectorpropActor = new Actor("selectorProp", selectorpropModel, new Vector3(0f,0f,0f), new Vector3(0f, 0f, 0f), new btSphereShape(.1f), 10f, LevelBuilder.CollisionCategory.nothing);
		sirLevel.instances.add(selectorpropActor.instance);
		sirLevel.actors.add(selectorpropActor);

		this.actorDiamond = selectorActor;
		this.actorCube = selectorcubeActor;
		this.actorPropSelect = selectorpropActor;

		// Set up input processing
		gestures = new GestureDetector(new GestureAdapter() {
			@Override
			public boolean tap(float x, float y, int count, int button) {
				Gdx.app.log("Sir!", "TAP - Count=" + count + " Button=" + button);
				if(count == 1) {
					if(button == 2) {
						Gdx.app.log("Sir!", "Middle click - Toggle mode");
						toggle_mode();
					}
				} else if(count == 2) {
					if(button == 0) {
						Gdx.app.log("Sir!", "Double click left - Count=" + count + " Button=" + button);
						switch(editMode) {
							case tile_mode:
								selector_add_tile();
								break;
							case prop_mode:
								selector_add_prop();
								break;
						}
					} else if(button == 1) {
						Gdx.app.log("Sir!", "Double click right - Count=" + count + " Button=" + button);
						switch(editMode) {
							case tile_mode:
								selector_remove_tile();
								break;
							case prop_mode:
								if(hovered_over_prop_index != -1) {
									selector_remove_prop(hovered_over_prop_index);
									hovered_over_prop_index = -1;
								}
								break;
						}
					}
				}

				return true;
			}

			@Override
			public boolean pan(float x, float y, float deltaX, float deltaY) {
				switch(button_pressed) {
					case 0:
						move_selector(deltaX,0,deltaY);
						break;
					case 1:
						move_selector(deltaX,-deltaY,0);
						break;
				}
				return false;
			}

			@Override
			public boolean touchDown(float x, float y, int pointer, int button) {
				button_pressed = button; // why in the name of fuck does GestureAdapter not do this shit for you?
				return false;
			}

			@Override
			public boolean panStop(float x, float y, int pointer, int button) {
				button_pressed = -1; // why in the name of fuck does GestureAdapter not do this shit for you?
				return false;
			}
		});

		keyboardInput = new InputAdapter() {
			@Override
			public boolean keyTyped(char character) {
				switch(character) {
					case 'h':
					case 'H':
						String s = "";
						s += "GENERAL:\n";
						s += "Left click drag: Moves selector diamond in the x-z plane.\n";
						s += "Right click drag: Moves selector diamond in the x-y plane.\n";
						s += "Shift+S: Saves current level to sir text file.\n";
						s += "Middle click OR Tab: toggles between TILEMODE and PROPMODE.\n";
						s += "Shift+P: Set player here.\n";
						s += "\n";
						s += "TILEMODE:\n";
						s += "Double left click: Places tile at selector position.\n";
						s += "Double right click: Deletes tile at selector position.\n";
						s += "A/D: Cycle left/right through tile selection.\n";
						s += "\n";
						s += "PROPMODE:\n";
						s += "Double left click: Drops prop from selector position.\n";
						s += "Double right click: Deletes prop mouse is over.\n";
						s += "A/D: Cycle left/right through prop selection.\n";
						s += "Z: Delete most recently dropped prop.\n";
						Gdx.app.log("Sir!", "H pressed - Help:\n" + s);
						break;

					case 'P':
						Gdx.app.log("Sir!", "Shift-P pressed - Moving kid start position here.");
						Vector3 posKid = new Vector3(diamond_x, diamond_y, diamond_z);
						placePlayer(posKid);
						break;

					case '\t':
						Gdx.app.log("Sir!", "Tab pressed - (toggle mode)");
						toggle_mode();
						break;

					case 'S':
						Gdx.app.log("Sir!", "Shift-S pressed - Saving txt file " + output_txt_fname);
						writeToFile(output_txt_fname);
						break;

					case 'z':
					case 'Z':
						Gdx.app.log("Sir!", "Z pressed - (delete last placed prop)");
						delete_last_placed_prop();
						break;

					case 'a':
					case 'A':
						Gdx.app.log("Sir!", "A pressed - (Cycle left through selection)");
						cycle_through_selection(-1);
						break;

					case 'd':
					case 'D':
						Gdx.app.log("Sir!", "D pressed - (Cycle right through selection)");
						cycle_through_selection(1);
						break;

					default:
				}
				return true;

			}

			@Override
			public boolean mouseMoved(int screenX, int screenY) {
/*
				if(editMode == EditMode.prop_mode) {
					Ray ray = Sir.chase.get_camera().getPickRay(screenX, screenY);
					Collidable col = sirLevel.physics.rayCasterQuadPlusXtreme(ray);
					if(col == null) {
						hovered_over_prop_index = -1;
					} else if(col.getCollisionCategory() == LevelBuilder.CollisionCategory.prop) {
						Actor delAct = col.getActor();
						hovered_over_prop_index = getCorrespondingPropIndex(delAct);
					} else {
						hovered_over_prop_index = -1;
					}
					updateSelectorPos();
				}
*/
				return false;
			}
		};

		// Set starting position at the middle of the map
		setDiamondCentre();

		return sirLevel;
	}

	/**
	 * Adds this object's controllers to the global input multiplexer
	 */
	public void handleInputs() {
		Sir.inputs.addProcessor(gestures);
		Sir.inputs.addProcessor(keyboardInput);
	}

	public void updateSelectorPos() {
		select_x = (int)(diamond_x/spacing +.5f);
		select_y = (int)(diamond_y/spacing);
		select_z = (int)(diamond_z/spacing +.5f);
		select_x = BC(select_x, size_x);
		select_y = BC(select_y, size_y);
		select_z = BC(select_z, size_z);

		Vector3 posDiamond = new Vector3(diamond_x, diamond_y, diamond_z);
		Vector3 posCube;
		Vector3 posProp = new Vector3(0.0f, diamond_y + 1000.0f, 0.0f);
		if(editMode == EditMode.tile_mode) {
			posCube = new Vector3(select_x * spacing, select_y * spacing, select_z * spacing);
		} else {
			// when not in tile edit mode, put the cube far away out of sight
			posCube = new Vector3(0.0f, diamond_y + 1000.0f, 0.0f);

			if(hovered_over_prop_index != -1) {
				posProp = added_props.get(hovered_over_prop_index).actor.physObj.getPos();
			}
		}
		this.setDiamondPos(posDiamond);
		this.setCubePos(posCube);
		this.setPropSelectPos(posProp);
	}

	/** Horrifically inefficient... */
	private int propIndexFromID(String match) {
		for(int i=0; i<prop_def.size; i++) {
			if(prop_def.get(i).id == match) {
				return i;
			}
		}
		return -1;
	}

	private int findTileIndex(String id, short rotation) {
		for(int i=0; i<tile_def.length; i++) {
			TILE_DEF td = tile_def[i];
			Gdx.app.log("Sir!", td.id + " " + td.rotation + " " + id + " " + rotation);
			if(td.id.trim().equals(id.trim()) && td.rotation == rotation) {
					return i;
			}
		}
		return -1;
	}

	private int getCorrespondingPropIndex(Actor delAct) {
		for(int i=0; i<added_props.size; i++) {
			if(added_props.get(i).actor == delAct) {
				return i;
			}
		}
		Gdx.app.log("Sir!", "Error - Could not find Prop.");
//		System.exit(0);
		return -1;
	}

	public void loadOld(String fname) {
		SirBinaryFileReader sbfr = new SirBinaryFileReader(fname);
		if (!sbfr.fileSuccessfullyLoaded()) {
			Gdx.app.error("Sir!", "Problem loading sir binary file.");
		}

		// Offset loaded level so that its centre is in the middle of our allocated space
		int[] dim = sbfr.calcBoundingBox();
		int margin_x = size_x - dim[0];
		int margin_z = size_z - dim[2];
		offset_x = (int)(margin_x/2.0);
		offset_z = (int)(margin_z/2.0);

		// Add tiles for all the floors
		Model all_tiles = Sir.assets.get(sbfr.getTilesFname());
		for(int f = 0; f < sbfr.getNumFloors(); f++) {
			final int floor_x = sbfr.get_floor_x(f);
			final int floor_y = sbfr.get_floor_y(f);
			final int first_tile_x = sbfr.get_first_tile_x(f);
			final int first_tile_y = sbfr.get_first_tile_y(f);

			for(int y = 0; y < floor_y; y++) {
				for(int x = 0; x < floor_x; x++) {
					if(sbfr.tileNotEmpty(f, x, y)) {
						String nodeId = sbfr.getTileId(f, x, y);
						short rotation = sbfr.getTileRotation(f, x, y);

						// Get the tile index corresponding to this tile
						tile_index = findTileIndex(nodeId, rotation);
						if(tile_index == -1) {
							Gdx.app.log("Sir!", "Error - Level Editor does not recognise the tile with id " + nodeId + " and rotation " + rotation);
							System.exit(0);
						}

						// set the tile selector to this tile position
						select_x = x + first_tile_x + offset_x;
						select_y = f;
						select_z = y + first_tile_y + offset_z;

						// place the tile
						selector_add_tile();
					}
				}
			}
		}

		// Build all the props
		for(int i = 0; i < sbfr.getNumProps(); i++) {
			// find the corresponding index in our level editor
			String id = sbfr.getPropId(i);
			LevelBuilder.PropState propstate = sbfr.getPropState(i);
			LevelBuilder.AIAlgo ai_algo = sbfr.getPropAI(i);

			prop_index = propIndexFromID(id);
			if(prop_index == -1) {
				// If unknown, add it to the list of level editor prop definitions
				prop_def.add(new PROP_DEF(id, propstate, ai_algo));
				prop_index = prop_def.size - 1;
			}

			// Move the selector to this position
			Vector3 pos = sbfr.getPropPosition(i);
			diamond_x = pos.x + offset_x * spacing;
			diamond_y = pos.y;
			diamond_z = pos.z + offset_z * spacing;

			// Add a prop here
			selector_add_prop();
		}

		// Spawn player
		Vector3 start = sbfr.getPlayerStartPosition();
		placePlayer(new Vector3(start.x + offset_x * spacing, start.y, start.z + offset_z * spacing));

		// Set starting position at the middle of the map
		setDiamondCentre();
	}

	public void move_selector(float vx, float vy, float vz) {
		final float displacement = .1f;
		Vector3 vel = new Vector3(vx, vy, vz);
		vel.nor().scl(displacement);
		diamond_x += vel.x;
		diamond_y += vel.y;
		diamond_z += vel.z;

		updateSelectorPos();
	}

	public Vector3 getDiamondPos() {
		return actorDiamond.physObj.getPos();
	}
	public Vector3 getCubePos() {
		return actorCube.physObj.getPos();
	}
	public Vector3 getPropSelectPos() {
		return actorPropSelect.physObj.getPos();
	}

	public void setDiamondPos(Vector3 pos) {
		Vector3 trans = new Vector3(pos);
		trans.sub(this.getDiamondPos());
		actorDiamond.instance.transform.trn(trans);
	}
	public void setCubePos(Vector3 pos) {
		Vector3 trans = new Vector3(pos);
		trans.sub(this.getCubePos());
		actorCube.instance.transform.trn(trans);
	}
	public void setPropSelectPos(Vector3 pos) {
		Vector3 trans = new Vector3(pos);
		trans.sub(this.getPropSelectPos());
		actorPropSelect.instance.transform.trn(trans);
	}

	public void placePlayer(Vector3 pos) {
		// Sigh... We have to remove the kid and re-add it in order to move a dynamic object
		if(actorKid != null) {
			sirLevel.instances.removeValue(actorKid.instance, true);
			sirLevel.actors.removeValue(actorKid, true);
			sirLevel.physics.removeCollidable(actorKid);
		}

		Model kidModel = Sir.assets.get("player/kid_game.g3db");
		actorKid = new Actor("player", kidModel, pos, new Vector3(0f,0f,0f), new btSphereShape(.5f), 10f, LevelBuilder.CollisionCategory.player);
		actorKid.disableInstanceRotation();
		sirLevel.instances.add(actorKid.instance);
		sirLevel.actors.add(actorKid);
		sirLevel.physics.addCollidable(actorKid, Physics.COL_PROP, Physics.COL_ALL, Physics.COL_NOTHING);
	}

	private int BC(int c, int max) {
		if (c < 0) {
			c = 0;
		} else if(c >= max) {
			c = max - 1;
		}
		return c;
	}

	public String toString() {
		String s;
		switch(editMode) {
			case tile_mode:
				s = "[TILE MODE] (" + select_x + " " + select_y + " " + select_z + ") [" + tile_def[tile_index].getTileDefString() + "]";
				s += ""+getDiamondPos();
				break;
			case prop_mode:
				s = "[PROP MODE] (" + select_x + " " + select_y + " " + select_z + ") [" + prop_def.get(prop_index) + "]";
				break;
			default:
				s = "Unknown mode";
		}
		return s;
	}

	public Actor getActor() {
		return actorDiamond;
	}

	/** toggle between tile mode and prop mode */
	private void toggle_mode() {
		if(editMode == EditMode.tile_mode) {
			editMode = EditMode.prop_mode;
			updateSelectorPos();
		} else if(editMode == EditMode.prop_mode) {
			editMode = EditMode.tile_mode;
			updateSelectorPos();
		}
		Gdx.app.log("Sir!", "Toggle:" + editMode);
	}

	private void cycle_through_selection(int incr) {
		switch(editMode) {
			case tile_mode:
				tile_index += incr;
				if(tile_index < 0) {
					tile_index = tile_def.length - 1;
				} else if(tile_index >= tile_def.length) {
					tile_index = 0;
				}
				break;
			case prop_mode:
				prop_index += incr;
				if(prop_index < 0) {
					prop_index = prop_def.size - 1;
				} else if(prop_index >= prop_def.size) {
					prop_index = 0;
				}
				break;
			default:
		}
	}

	private void setDiamondCentre() {
		diamond_x = (size_x/2) * spacing;
		diamond_y = 5 + spacing * .5f;
		diamond_z = (size_z/2) * spacing;

		updateSelectorPos();
	}

	private void selector_add_tile() {
		TileROFL99 tr99 = tiles[select_x][select_y][select_z];

		// Check if tile is replacing an existing tile (which must therefore be deleted)
		if(tr99.tile_index != -1) {
			selector_remove_tile();
		}

		tr99.tile_index = tile_index;

		Model all_tiles = Sir.assets.get("tiles/restaurant.g3db");
		btCollisionShape colshape = PhysObject.createConvexHullShape(all_tiles, tile_def[tile_index].id, true);
		Vector3 pos = new Vector3(select_x * spacing, select_y * spacing, select_z * spacing);
		Vector3 rot = new Vector3(0,0, tile_def[tile_index].rotation);
		Actor newActor = new Actor("tile", all_tiles, tile_def[tile_index].id, pos, rot, colshape, 0f, tile_def[tile_index].collisionCategory);
		tr99.actor = newActor;

		sirLevel.instances.add(newActor.instance);
		sirLevel.actors.add(newActor);
		sirLevel.physics.addCollidable(newActor, Physics.COL_TILE, Physics.COL_DYNAMIC_AND_RAYCASTABLE, Physics.COL_NOTHING);
	}

	private void selector_add_prop() {
		Gdx.app.log("Sir!", "Place prop - " + prop_def.get(prop_index).toString());

		String propId = prop_def.get(prop_index).id;
		String propFname = "props/" + propId + ".g3db";
		LevelBuilder.PropState propstate = prop_def.get(prop_index).propstate;
		Model propModel = Sir.assets.get(propFname);
		Vector3 proppos = new Vector3(diamond_x, diamond_y, diamond_z);
		Vector3 proprot = new Vector3(0f, 0f, 0f);
		Actor propnewActor;
		propnewActor = new Actor(propId, propModel, proppos, proprot, propstate, LevelBuilder.CollisionCategory.prop);
		propnewActor.disableInstanceRotation();
		sirLevel.instances.add(propnewActor.instance);
		sirLevel.actors.add(propnewActor);
		sirLevel.physics.addCollidable(propnewActor, Physics.COL_PROP, Physics.COL_ALL, Physics.COL_NOTHING);

		added_props.add(new PropROFL99(propnewActor, prop_index));
	}

	private void selector_remove_tile() {
		Gdx.app.log("Sir!", "Remove tile - " + tile_def[tile_index].toString());

		TileROFL99 tr99 = tiles[select_x][select_y][select_z];
		if(tr99.tile_index != -1) {
			sirLevel.instances.removeValue(tr99.actor.instance, true);
			sirLevel.actors.removeValue(tr99.actor, true);
			sirLevel.physics.removeCollidable(tr99.actor);
			tr99.tile_index = -1;
		}
	}

	private void selector_remove_prop(int pi) {
		Gdx.app.log("Sir!", "Attempt - " + pi + "/" + added_props.size);
		if(pi < 0 || pi >= added_props.size) {
			return;
		}

		PropROFL99 pr99 = added_props.get(pi);
		Gdx.app.log("Sir!", "Remove prop - " + prop_def.get(pr99.prop_index).toString());
		Actor act = pr99.actor;
		sirLevel.instances.removeValue(act.instance, true);
		sirLevel.actors.removeValue(act, true);
		sirLevel.physics.removeCollidable(act);
		added_props.removeValue(pr99, true);
	}

	private void delete_last_placed_prop() {
		if(added_props.size == 0) {
			Gdx.app.log("Sir!", "Can't delete prop. No props to delete.");
			return;
		}
		selector_remove_prop(added_props.size-1);
	}

	public void writeToFile(String fname) {
		// Work out the bounding volume that contains all non-empty tiles
		int min_x=Integer.MAX_VALUE, min_y=Integer.MAX_VALUE, min_z=Integer.MAX_VALUE, max_x=-1, max_y=-1, max_z=-1;
		for(int z = 0; z < size_z; z++) {
			for(int y = 0; y < size_y; y++) {
				for(int x = 0; x < size_x; x++) {
					if(tiles[x][y][z].tile_index != -1) {
						if(x < min_x) {
							min_x = x;
						} else if(x > max_x) {
							max_x = x;
						}
						if(y < min_y) {
							min_y = y;
						} else if(y > max_y) {
							max_y = y;
						}
						if(z < min_z) {
							min_z = z;
						} else if(z > max_z) {
							max_z = z;
						}
					}
				}
			}
		}

		Writer writer = null;
		try {
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fname), "utf-8"));

			String ls = "";
			ls += "FILES:\ntiles = restaurant.g3db\n\n";
			ls += "PLAYER:\n";

			if(actorKid != null) {
				Vector3 playpos = actorKid.physObj.getPos();
				ls += "start " + (playpos.x - min_x*spacing) + " " + (playpos.y - min_y*spacing) + " " + (playpos.z - min_z*spacing) + "\n\n";
			} else {
				ls += "start 0.0 20.0 0.0\n\n";
			}

			ls += "TILES:\n";
			ls += "@ ORIGIN\n";
			for(int i = 0; i < tile_def.length; i++) {
				ls += tile_def[i].getTileDefString() + "\n";
			}
			ls += "\n";

			if(min_x == -1) {
				Gdx.app.log("Sir!", "Warning - No tiles in map...");
				ls += "MAP:\n";
				ls += "@\n";
			} else {
				Gdx.app.log("Sir!", "Bounding box:" + min_x + "->" + max_x + ", " + min_y + "->" + max_y + ", " + min_z + "->" + max_z);

				// Write each floor within the bounding box
				for(int y = min_y; y <= max_y; y++) {
					ls += "MAP:\n";
					ls += "@\n";
					for(int z = min_z; z <= max_z; z++) {
						for(int x = min_x; x <= max_x; x++) {
							if(tiles[x][y][z].tile_index == -1) {
								ls += " ";
							} else {
								ls += tile_def[tiles[x][y][z].tile_index].txt_char;
							}
						}
						ls += "\n";
					}
				}
			}
			ls += "\n";
			ls += "PROPS:\n";
			for(PropROFL99 pr99 : added_props) {
				PROP_DEF pd = prop_def.get(pr99.prop_index);
				Vector3 pos = pr99.actor.physObj.getPos();
				String pstate = "";
				switch(pd.propstate) {
					case dynamicprop:
						pstate = "dynamic";
						break;
					case quasiprop:
						pstate = "quasi";
						break;
					case staticprop:
						pstate = "static";
						break;
					default:
				}
				ls+=pd.id + " " + pos.x + " " + pos.y + " " + pos.z + " 0 " + pstate + " " + pd.ai_algo + "\n";
			}
			writer.write(ls);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {writer.close();} catch (Exception e) {/*ignore*/}
		}
	}
}
