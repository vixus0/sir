#!/usr/bin/env python

import bpy
import sys

argv = sys.argv
argv = argv[argv.index("--") + 1:]

fbx_out = argv[0]

bpy.ops.export_scene.fbx(filepath=fbx_out, axis_forward='-X', axis_up='Z')
